require 'test_helper'

class BoyacaConfigsControllerTest < ActionController::TestCase
  setup do
    @boyaca_config = boyaca_configs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:boyaca_configs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create boyaca_config" do
    assert_difference('BoyacaConfig.count') do
      post :create, boyaca_config: { option_name: @boyaca_config.option_name, option_value: @boyaca_config.option_value }
    end

    assert_redirected_to boyaca_config_path(assigns(:boyaca_config))
  end

  test "should show boyaca_config" do
    get :show, id: @boyaca_config
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @boyaca_config
    assert_response :success
  end

  test "should update boyaca_config" do
    put :update, id: @boyaca_config, boyaca_config: { option_name: @boyaca_config.option_name, option_value: @boyaca_config.option_value }
    assert_redirected_to boyaca_config_path(assigns(:boyaca_config))
  end

  test "should destroy boyaca_config" do
    assert_difference('BoyacaConfig.count', -1) do
      delete :destroy, id: @boyaca_config
    end

    assert_redirected_to boyaca_configs_path
  end
end
