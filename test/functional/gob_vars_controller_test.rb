require 'test_helper'

class GobVarsControllerTest < ActionController::TestCase
  setup do
    @gob_var = gob_vars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gob_vars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gob_var" do
    assert_difference('GobVar.count') do
      post :create, gob_var: { meta_key: @gob_var.meta_key, meta_value: @gob_var.meta_value }
    end

    assert_redirected_to gob_var_path(assigns(:gob_var))
  end

  test "should show gob_var" do
    get :show, id: @gob_var
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gob_var
    assert_response :success
  end

  test "should update gob_var" do
    put :update, id: @gob_var, gob_var: { meta_key: @gob_var.meta_key, meta_value: @gob_var.meta_value }
    assert_redirected_to gob_var_path(assigns(:gob_var))
  end

  test "should destroy gob_var" do
    assert_difference('GobVar.count', -1) do
      delete :destroy, id: @gob_var
    end

    assert_redirected_to gob_vars_path
  end
end
