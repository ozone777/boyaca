require 'test_helper'

class ComplaintsControllerTest < ActionController::TestCase
  setup do
    @complaint = complaints(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:complaints)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create complaint" do
    assert_difference('Complaint.count') do
      post :create, complaint: { auto_acomulatorio_id: @complaint.auto_acomulatorio_id, comaplaint_type_id: @complaint.comaplaint_type_id, comentario_auto_inhibitorio: @complaint.comentario_auto_inhibitorio, comentarios_jefe_oficina: @complaint.comentarios_jefe_oficina, estado_proceso: @complaint.estado_proceso, fecha_hechos: @complaint.fecha_hechos, fecha_vencimiento_terminos: @complaint.fecha_vencimiento_terminos, texto_indagacion_preliminar: @complaint.texto_indagacion_preliminar }
    end

    assert_redirected_to complaint_path(assigns(:complaint))
  end

  test "should show complaint" do
    get :show, id: @complaint
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @complaint
    assert_response :success
  end

  test "should update complaint" do
    put :update, id: @complaint, complaint: { auto_acomulatorio_id: @complaint.auto_acomulatorio_id, comaplaint_type_id: @complaint.comaplaint_type_id, comentario_auto_inhibitorio: @complaint.comentario_auto_inhibitorio, comentarios_jefe_oficina: @complaint.comentarios_jefe_oficina, estado_proceso: @complaint.estado_proceso, fecha_hechos: @complaint.fecha_hechos, fecha_vencimiento_terminos: @complaint.fecha_vencimiento_terminos, texto_indagacion_preliminar: @complaint.texto_indagacion_preliminar }
    assert_redirected_to complaint_path(assigns(:complaint))
  end

  test "should destroy complaint" do
    assert_difference('Complaint.count', -1) do
      delete :destroy, id: @complaint
    end

    assert_redirected_to complaints_path
  end
end
