require 'test_helper'

class GobDataControllerTest < ActionController::TestCase
  setup do
    @gob_datum = gob_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gob_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gob_datum" do
    assert_difference('GobDatum.count') do
      post :create, gob_datum: { dpto: @gob_datum.dpto, name: @gob_datum.name }
    end

    assert_redirected_to gob_datum_path(assigns(:gob_datum))
  end

  test "should show gob_datum" do
    get :show, id: @gob_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gob_datum
    assert_response :success
  end

  test "should update gob_datum" do
    put :update, id: @gob_datum, gob_datum: { dpto: @gob_datum.dpto, name: @gob_datum.name }
    assert_redirected_to gob_datum_path(assigns(:gob_datum))
  end

  test "should destroy gob_datum" do
    assert_difference('GobDatum.count', -1) do
      delete :destroy, id: @gob_datum
    end

    assert_redirected_to gob_data_path
  end
end
