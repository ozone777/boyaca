require 'test_helper'

class ControlInternoDataControllerTest < ActionController::TestCase
  setup do
    @control_interno_datum = control_interno_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:control_interno_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create control_interno_datum" do
    assert_difference('ControlInternoDatum.count') do
      post :create, control_interno_datum: { name: @control_interno_datum.name }
    end

    assert_redirected_to control_interno_datum_path(assigns(:control_interno_datum))
  end

  test "should show control_interno_datum" do
    get :show, id: @control_interno_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @control_interno_datum
    assert_response :success
  end

  test "should update control_interno_datum" do
    put :update, id: @control_interno_datum, control_interno_datum: { name: @control_interno_datum.name }
    assert_redirected_to control_interno_datum_path(assigns(:control_interno_datum))
  end

  test "should destroy control_interno_datum" do
    assert_difference('ControlInternoDatum.count', -1) do
      delete :destroy, id: @control_interno_datum
    end

    assert_redirected_to control_interno_data_path
  end
end
