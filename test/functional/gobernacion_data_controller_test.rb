require 'test_helper'

class GobernacionDataControllerTest < ActionController::TestCase
  setup do
    @gobernacion_datum = gobernacion_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gobernacion_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gobernacion_datum" do
    assert_difference('GobernacionDatum.count') do
      post :create, gobernacion_datum: { dpto: @gobernacion_datum.dpto, name: @gobernacion_datum.name }
    end

    assert_redirected_to gobernacion_datum_path(assigns(:gobernacion_datum))
  end

  test "should show gobernacion_datum" do
    get :show, id: @gobernacion_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gobernacion_datum
    assert_response :success
  end

  test "should update gobernacion_datum" do
    put :update, id: @gobernacion_datum, gobernacion_datum: { dpto: @gobernacion_datum.dpto, name: @gobernacion_datum.name }
    assert_redirected_to gobernacion_datum_path(assigns(:gobernacion_datum))
  end

  test "should destroy gobernacion_datum" do
    assert_difference('GobernacionDatum.count', -1) do
      delete :destroy, id: @gobernacion_datum
    end

    assert_redirected_to gobernacion_data_path
  end
end
