require 'test_helper'

class NulidadsControllerTest < ActionController::TestCase
  setup do
    @nulidad = nulidads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nulidads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nulidad" do
    assert_difference('Nulidad.count') do
      post :create, nulidad: { name: @nulidad }
    end

    assert_redirected_to nulidad_path(assigns(:nulidad))
  end

  test "should show nulidad" do
    get :show, id: @nulidad
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nulidad
    assert_response :success
  end

  test "should update nulidad" do
    put :update, id: @nulidad, nulidad: { name: @nulidad }
    assert_redirected_to nulidad_path(assigns(:nulidad))
  end

  test "should destroy nulidad" do
    assert_difference('Nulidad.count', -1) do
      delete :destroy, id: @nulidad
    end

    assert_redirected_to nulidads_path
  end
end
