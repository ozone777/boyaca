require 'test_helper'

class GobDocsControllerTest < ActionController::TestCase
  setup do
    @gob_doc = gob_docs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gob_docs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gob_doc" do
    assert_difference('GobDoc.count') do
      post :create, gob_doc: { dpto: @gob_doc.dpto, name: @gob_doc.name }
    end

    assert_redirected_to gob_doc_path(assigns(:gob_doc))
  end

  test "should show gob_doc" do
    get :show, id: @gob_doc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gob_doc
    assert_response :success
  end

  test "should update gob_doc" do
    put :update, id: @gob_doc, gob_doc: { dpto: @gob_doc.dpto, name: @gob_doc.name }
    assert_redirected_to gob_doc_path(assigns(:gob_doc))
  end

  test "should destroy gob_doc" do
    assert_difference('GobDoc.count', -1) do
      delete :destroy, id: @gob_doc
    end

    assert_redirected_to gob_docs_path
  end
end
