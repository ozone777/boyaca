require 'test_helper'

class OfilesControllerTest < ActionController::TestCase
  setup do
    @ofile = ofiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ofiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ofile" do
    assert_difference('Ofile.count') do
      post :create, ofile: { name: @ofile.name }
    end

    assert_redirected_to ofile_path(assigns(:ofile))
  end

  test "should show ofile" do
    get :show, id: @ofile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ofile
    assert_response :success
  end

  test "should update ofile" do
    put :update, id: @ofile, ofile: { name: @ofile.name }
    assert_redirected_to ofile_path(assigns(:ofile))
  end

  test "should destroy ofile" do
    assert_difference('Ofile.count', -1) do
      delete :destroy, id: @ofile
    end

    assert_redirected_to ofiles_path
  end
end
