function paypal(_production){
	var url_base = "https://api.sandbox.paypal.com"
	if (_production){
		var url_base = "https://api.paypal.com"
	}
	this.validate_paypal = function(client_api, promise){
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			error: promise,
			success: promise
		})
	}
	this.set_billing_plan = function(client_api, data){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function(result){
				api_token = result.access_token;
				$.ajax({
					url: url_base + "/v1/payments/billing-plans",
					type: "POST",
					dataType: "JSON",
					headers: {
						"Content-Type": "application/json",
						"Authorization": "Bearer " + api_token,
						'Accept': 'application/json'
					},
					data: JSON.stringify(data)
				}).done(function(result){
					$.ajax({
						type: "PATCH",
						url: result.links[0].href,
						headers: {
							"Content-Type": "application/json",
							"Authorization": "Bearer " +api_token
						},
						data: '[{"path": "/","value": {"state": "ACTIVE"},"op": "replace"}]',
						statusCode: {
							200: function(){
								var fecha = new Date(new Date().getTime() + 1000*60*20);
								var newFecha = fecha.getUTCFullYear();
								if ( (fecha.getUTCMonth() + 1) >= 10 )
								newFecha += "-"+(fecha.getUTCMonth()+1)
								else
								newFecha += "-0" + (fecha.getUTCMonth()+1)
								if ( fecha.getUTCDate() >= 10 )
								newFecha += "-"+fecha.getUTCDate() +"T"
								else
								newFecha += "-0" + fecha.getUTCDate()+"T"
								if ( fecha.getUTCHours() >= 10 )
								newFecha += (fecha.getUTCHours())
								else
								newFecha += "0" + (fecha.getUTCHours())
								if ( fecha.getUTCMinutes() >= 10 )
								newFecha += ":"+ fecha.getUTCMinutes()
								else
								newFecha += ":0" + fecha.getUTCMinutes()
								if ( fecha.getUTCSeconds() >= 10)
								newFecha += ":"+fecha.getUTCSeconds()+"Z"
								else
								newFecha += ":0" + fecha.getUTCSeconds()+"Z"
								$.ajax({
									type: "POST",
									url: url_base + "/v1/payments/billing-agreements",
									headers: {
										"Content-Type": "application/json",
										"Authorization": "Bearer " + api_token
									},
									data: JSON.stringify(
										{
											"name": data.name, 
											"description": "Agreement for " + data.name,
											"start_date": newFecha,
											"plan": { 
												"id": result.id
											},
											"payer": { 
												"payment_method": "paypal" 
											}
										}
									),
									success: function(result){
										window.location.href = result.links[0].href;
									}
								})
							}
						}
					});
				});
			}
		});
	}
	this.check_billing_plan = function(client_api, id_plan, url){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function( result ){
				api_token = result.access_token;
				$.ajax({
					type: "GET",
					url: url_base + "/v1/payments/billing-agreements/"+id_plan,
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					success: function(result){
						$.ajax({
							type: "GET",
							url: url,
							dataType: "json",
							data: {
								'paypal_id': result.id,
								'pro_plan': result.plan.payment_definitions[0].amount.value,
								'status': result.state
							},
							success: function(response){
							}
						});
					}
				});
			}
		});
	}
	this.cancel_billing_plan = function(client_api, id_plan, url){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function( result ){
				api_token = result.access_token;
				$.ajax({
					type: "POST",
					url: url_base + "/v1/payments/billing-agreements/"+id_plan+"/cancel",
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					data: '{"note": "Canceling the agreement."}',
					success: function(result){
						console.log(result)
						$.ajax({
							type: "GET",
							url: url,
							dataType: "json",
							data: {
								'paypal_id': result.id,
								'pro_plan': result.plan.payment_definitions[0].amount.value,
								'status': result.state
							},
							success: function(response){
							}
						});
					}
				});
			}
		});
	}
	this.suspend_billing_plan = function(client_api, id_plan, url){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function( result ){
				api_token = result.access_token;
				$.ajax({
					type: "POST",
					url: url_base + "/v1/payments/billing-agreements/"+id_plan+"/suspend",
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					data: '{"note": "Canceling the agreement."}',
					success: function(result){
						console.log(result)
						$.ajax({
							type: "GET",
							url: url,
							dataType: "json",
							data: {
								'paypal_id': result.id,
								'pro_plan': result.plan.payment_definitions[0].amount.value,
								'status': result.state
							},
							success: function(response){
							}
						});
					}
				});
			}
		});
	}
	this.execute_billing_plan = function(client_api, token, url){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function( result ){
				api_token = result.access_token;
				$.ajax({
					type: "POST",
					url: url_base + "/v1/payments/billing-agreements/"+token+"/agreement-execute",
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					data: '{}',
					success: function(result){
						$.ajax({
							type: "GET",
							url: result.links[0].href,
							dataType: "json",
							headers: {
								'Content-Type': 'application/json',
								"Authorization": "Bearer " + api_token
							},
							success: function(result){
								$.ajax({
									type: "GET",
									url: url,
									dataType: "json",
									data: {
										'paypal_id': result.id,
										'pro_plan': result.plan.payment_definitions[0].amount.value,
										'status': result.state
									},
									success: function(response){
									}
								});
							}
						});
					}
				});
			}
		});
	}
	this.set_payment = function(client_api, data){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function(result){
				api_token = result.access_token;
				$.ajax({
					type: "POST",
					url: url_base + "/v1/payments/payment",
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					data: JSON.stringify(data),
					success: function(result){
						$.ajax({
							type: "GET",
							url: "/set_paypal_payment_for_course",
							dataType: "json",
							data: {
								"paymet_id": result.id
							},
							success: function(){
								window.location.href = result.links[1].href;
							}
						});
					}
				});
			}
		});
	}
	this.execute_payment = function(client_api, payer_id, token, url, course_id){
		var api_token = null;
		$.ajax({
			type: "POST",
			url: url_base + "/v1/oauth2/token",
			dataType: "json",
			headers: {
				"Accept-Language": "en_US",
				'Accept': 'application/json',
				"Authorization": "Basic " + client_api
			},
			data: "grant_type=client_credentials",
			success: function( result ){
				api_token = result.access_token;
				$.ajax({
					type: "POST",
					url: url_base + "/v1/payments/payment/"+token+"/execute",
					dataType: "json",
					headers: {
						'Content-Type': 'application/json',
						"Authorization": "Bearer " + api_token
					},
					data: JSON.stringify(
						{
							"payer_id" : payer_id 
						}
					),
					success: function(result){
						$.ajax({
							type: "GET",
							url: url,
							dataType: "json",
							data: {
								'course_id':course_id
							},
							success: function(response){
								window.location.href = response.url;
							}
						});
					}
				});
			}
		});
	}
}