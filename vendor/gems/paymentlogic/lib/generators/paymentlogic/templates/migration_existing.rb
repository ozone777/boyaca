class AddPaymentlogicTo<%= table_name.camelize %> < ActiveRecord::Migration
  def change
<%= migration_data -%>
<% @attributes.each do |attribute| -%>
      add_column :<%= table_name %>, :<%= attribute.name %>, :<%= attribute.type %> 
<% end -%>
  end
end
