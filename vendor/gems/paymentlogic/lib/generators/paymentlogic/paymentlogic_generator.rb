require 'rails/generators/named_base'
require 'rails/generators/migration'
require 'rails/generators/active_record'

class PaymentlogicGenerator < Rails::Generators::NamedBase
  include Rails::Generators::Migration
  
      desc "Generates a model with the given NAME (if one does not exist) with devise " <<
           "configuration plus a migration file and devise routes."
  
  namespace "paymentlogic"
  class_option :routes, desc: "Generate routes", type: :boolean, default: true
  source_root File.expand_path('../templates', __FILE__)  
  #Permitir recibir parametros desde consola hola:string eail:string ....
  argument :model_attributes, type: :array, default: [], banner: "model:attributes"
  #Permite fomatear los parametros y ponga string por default
  def initialize(*args, &block)
    super
    @attributes = []
    model_attributes.each do |attribute|
      @attributes << Rails::Generators::GeneratedAttribute.new(*attribute.split(":"))
    end
  end
  
# Ejemplo real
# rails g paymentlogic admin name:string tel:integer
# @attributes = [{ name:name, type:string }, { name:tel, type:integer }] 
  def generate_model
    invoke "active_record:model", [name], migration: false unless model_exists? && behavior == :invoke
  end
  
  def inject_payment_content
    content = model_data

    class_path = if namespaced?
      class_name.to_s.split("::")
    else
      [class_name]
    end

    indent_depth = class_path.size - 1
    content = content.split("\n").map { |line| "  " * indent_depth + line } .join("\n") << "\n"
    inject_into_class(model_path, class_path.last, content) if model_exists?
  end
  
  def copy_payment_migration
    if (behavior == :invoke && model_exists?) || (behavior == :revoke && migration_exists?(table_name))
      migration_template "migration_existing.rb", "db/migrate/add_paymentlogic_to_#{table_name}.rb"
    else
      migration_template "migration.rb", "db/migrate/paymentlogic_create_#{table_name}.rb"
    end
  end
  
  def copy_payment_controller
    create_file "app/controllers/paymentlogics_controller.rb", controller_data
  end
  
  def copy_payment_view
   copy_file "thank_you_for_your_payment.html.erb", "app/views/paymentlogics/thank_you_for_your_payment.html.erb"
  end
  
  def add_payment_routes
    payment_route  = "match 'thank_you_for_your_payment'  => 'paymentlogics#thank_you_for_your_payment', :via => 'get'"
    route payment_route
    payment_route  = "match 'set_paypal_payment'  => 'paymentlogics#set_paypal_payment', :via => 'get'"
    route payment_route
    payment_route  = "match 'set_paypal_payment_for_course' => 'paymentlogics#set_paypal_payment_for_course'"
    route payment_route
  end
    
  private  
  def self.next_migration_number(path)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end
  
  def model_data
<<RUBY

    attr_accessible :payment_date, :payment_status, :payment_id

RUBY
  end
  
  def model_exists?
    File.exists?(File.join(destination_root, model_path))
  end

  def migration_exists?(table_name)
    Dir.glob("#{File.join(destination_root, migration_path)}/[0-9]*_*.rb").grep(/\d+_add_devise_to_#{table_name}.rb$/).first
  end

  def migration_path
    @migration_path ||= File.join("db", "migrate")
  end

  def table_name
    name
  end

  def model_path
    @model_path ||= File.join("app", "models", "#{file_path}.rb")
  end

  def migration_data
<<RUBY
    add_column :users, :payment_date, :string
    add_column :users, :payment_status, :integer
    add_column :users, :payment_id, :string
    add_column :users, :paypal_payment_id, :string
RUBY
  end
  
   def controller_data
<<RUBY
class PaymentlogicsController < ApplicationController
  def set_paypal_payment
    #Actualiza el estado del plan

    proplan = params[:pro_plan]
    status = params[:status]
    payment_date = nil
    if status == "Suspend"
      pro_status = 3      
    elsif status == "Pending"
      pro_status = 2
      payment_date = DateTime.now
    elsif status == "Active"
      pro_status = 1
      pro = true
      payment_date = DateTime.now + 1.month
    end

    #############################################################
    #--------Logica para el campo pro_status en User -----------#
    #-----------------------------------------------------------#
    #--null. nunca ha realizado una transaccion-----------------#
    #--1. Active -----------------------------------------------#
    #--2. Pending ----------------------------------------------#
    #--3. Suspend ----------------------------------------------#
    #-----------------------------------------------------------#
    #-----------------------------------------------------------#
    #############################################################

    puts proplan
    if current_user
      u = User.find(current_user.id)
      u.paypal_payment_id = params[:paypal_id]
      u.pro_plan = proplan
      u.pro_status = pro_status
      u.payment_date = payment_date
      if u.save                                            
        respond_to do |format|                            
          format.json {render json: { "OK" => "OK"}}
        end
      end
    end
  end
  
  def set_paypal_payment_for_course
    if current_user
      u = User.find(current_user.id)
      u.payment_id = params[:paymet_id]
      u.save!
      u.show_the_errors
    end
    respond_to do |format|
      format.json { render json: {} }
    end
  end

  def thank_you_for_your_payment
    @tokens = {}
    @tokens[:token] = params[:token]
    @tokens[:PayerID] = params[:PayerID]
    @billing = true
    if params[:course_id]
      @billing = false
      @course = Group.find(params[:course_id])
      @course_owner = User.find(@course.owner_id)
      user = User.find(current_user)
      @tokens[:paymet_id] = user.payment_id
      @tokens[:course_id] = params[:course_id]
      @client_api = Base64.encode64(@course_owner.paypal_app_id + ":" + @course_owner.paypal_client_secret).gsub(/\n/, '')
    else
      @client_api = Base64.encode64(@teachstars_config.paypal_app_id + ":" + @teachstars_config.paypal_client_secret).gsub(/\n/, '')
    end

  end
end
RUBY
  end
  
end
