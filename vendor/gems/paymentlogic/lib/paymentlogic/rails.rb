require 'paymentlogic/rails/engine'

module Paymentlogic
  
  module Rails
    
  end

  def self.encode_paypal_key(hash={})
    Base64.encode64(hash[:app_id]+ ":" + hash[:secret_client]).gsub(/\n/, '')
  end
  
end

class Addpaymentlogc < ActiveRecord::Migration
  def self.add_fields
    add_column :users, :xxxxxxxx, :integer
    add_column :users, :course_payment_id, :string
    add_column :users, :pro_status, :integer
  end
end
