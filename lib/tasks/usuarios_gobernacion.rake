# encoding: utf-8
namespace :admin  do
  desc "create some fake data"
  task :usuarios_gobernacion => :environment do
  
    #User.delete_all
    #Roles
    #5 Administrador
    #1 Auxiliar Administrativo (Crea quejas)
    #2 Jefe oficina
    #3 Profesional
    
    User.delete_all
    
    user = User.new
    user.id = 1
    user.first_name = "admin"
    user.last_name = "ozone"
    user.company = "Ozone"
    user.email = "software@ozonegroup.co"
    user.password = "pixma301"
    user.password_confirmation = "pixma301" 
    user.role_id = 5
    user.save
    
    user = User.new
    user.id = 2
    user.first_name = "Will"
    user.last_name = "Amaya"
    user.company = "Gobernación de Boyaca"
    user.email = "director.sistemas@boyaca.gov.co"
    user.password = "pixma301"
    user.password_confirmation = "pixma301" 
    user.role_id = 5
    user.save
    
    #auxiliares administrativos
    user = User.new
    user.id = 3
    user.first_name = "ERIKA"
    user.last_name = "GARAY"
    user.company = "Gobernación de Boyaca"
    user.email = "erikaandreagc11@hotmail.com"
    user.password = "boyaca301"
    user.password_confirmation = "boyaca301" 
    user.role_id = 1
    user.save
    
    user = User.new
    user.id = 4
    user.first_name = "STELLA"
    user.last_name = "RODRIGUEZ"
    user.company = "Gobernación de Boyaca"
    user.email = "stellarosuesca@yahoo.com"
    user.password = "boyaca235"
    user.password_confirmation = "boyaca235" 
    user.role_id = 1
    user.save
    
    user = User.new
    user.id = 5
    user.first_name = "MONICA"
    user.last_name = "MURILLO"
    user.company = "Gobernación de Boyaca"
    user.email = "monimurillo8906@hotmail.com"
    user.password = "boyaca127"
    user.password_confirmation = "boyaca127" 
    user.role_id = 1
    user.save
    
    user = User.new
    user.id = 6
    user.first_name = "KAREN"
    user.last_name = "PEREZ"
    user.company = "Gobernación de Boyaca"
    user.email = "kalipepi93@gmail.com"
    user.password = "boyaca310"
    user.password_confirmation = "boyaca310" 
    user.role_id = 1
    user.save
    
    #profesionales
    user = User.new
    user.id = 7
    user.first_name = "STELLA"
    user.last_name = "LEGUIZAMON"
    user.company = "Gobernación de Boyaca"
    user.email = "stellaagente007@yahoo.es"
    user.password = "boyaca987"
    user.password_confirmation = "boyaca987" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 8
    user.first_name = "EDWIN"
    user.last_name = "FONSECA"
    user.company = "Gobernación de Boyaca"
    user.email = "dredwin214@hotmail.com"
    user.password = "boyaca124"
    user.password_confirmation = "boyaca124" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 9
    user.first_name = "KAREN"
    user.last_name = "PEREZ"
    user.company = "Gobernación de Boyaca"
    user.email = "kalipepi93@gmail.com"
    user.password = "boyaca310"
    user.password_confirmation = "boyaca310" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 10
    user.first_name = "ANGELA"
    user.last_name = "SACRISTAN"
    user.company = "Gobernación de Boyaca"
    user.email = "angelitasari_13@hotmail.com"
    user.password = "boyaca098"
    user.password_confirmation = "boyaca098" 
    user.role_id = 2
    user.save
    
    user = User.new
    user.id = 11
    user.first_name = "FREDY"
    user.last_name = "PEREZ"
    user.company = "Gobernación de Boyaca"
    user.email = "fredyopv@hotmail.com"
    user.password = "boyaca077"
    user.password_confirmation = "boyaca077" 
    user.role_id = 3
    user.save
    
    
    user = User.new
    user.id = 12
    user.first_name = "WILLIAM"
    user.last_name = "GOYENECHE"
    user.company = "Gobernación de Boyaca"
    user.email = "dionisiogoyeneche@hotmail.com"
    user.password = "boyaca456"
    user.password_confirmation = "boyaca456" 
    user.role_id = 3
    user.save
    
    
    user = User.new
    user.id = 13
    user.first_name = "LUIS"
    user.last_name = "BERNAL"
    user.company = "Gobernación de Boyaca"
    user.email = "lcbm1005@hotmail.com"
    user.password = "boyaca976"
    user.password_confirmation = "boyaca976" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 14
    user.first_name = "LAURA"
    user.last_name = "ALFONSO"
    user.company = "Gobernación de Boyaca"
    user.email = "dumbito7@hotmail.com"
    user.password = "boyaca173"
    user.password_confirmation = "boyaca173" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 15
    user.first_name = "JUAN"
    user.last_name = "HERNANDEZ"
    user.company = "Gobernación de Boyaca"
    user.email = "juancarloshernandezabog@gmail.com"
    user.password = "boyaca234"
    user.password_confirmation = "boyaca234" 
    user.role_id = 2
    user.save
    
    user = User.new
    user.id = 16
    user.first_name = "JHONNY"
    user.last_name = "ORTIZ"
    user.company = "Gobernación de Boyaca"
    user.email = "jhonisito@gmail.com"
    user.password = "boyaca434"
    user.password_confirmation = "boyaca434" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 17
    user.first_name = "ADRIANA"
    user.last_name = "FAJARDO"
    user.company = "Gobernación de Boyaca"
    user.email = "adricons10@hotmail.com"
    user.password = "boyaca190"
    user.password_confirmation = "boyaca190" 
    user.role_id = 3
    user.save
    
    user = User.new
    user.id = 18
    user.first_name = "NIDIA"
    user.last_name = "BARON"
    user.company = "Gobernación de Boyaca"
    user.email = "milenabar76@hotmail.com"
    user.password = "boyaca278"
    user.password_confirmation = "boyaca278" 
    user.role_id = 3
    user.save
    
  
end

end