# encoding: utf-8
require 'faker'

namespace :admin  do
  desc "create some fake data"
  task :fake_data => :environment do
    
    User.delete_all
  
    #Roles
    #5 Administrador
    #1 Auxiliar Administrativo (Crea quejas)
    #2 Jefe oficina
    #3 Profesional
    
    User.delete_all
    user = User.new
    user.first_name = "admin"
    user.last_name = "ozone"
    user.company = "Ozone"
    user.email = "software@ozonegroup.co"
    user.password = "pixma301"
    user.password_confirmation = "pixma301" 
    user.role_id = 5
    user.save
  
    #Administrador
    user = User.new
    user.first_name = "Bryan"
    user.last_name = "Villafañe"
    user.company = "Ozone"
    user.email = "brian.villaf@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 5
    user.save

    #Auxiliar administrativo
    user = User.new
    user.first_name = "Laura"
    user.last_name = "Perez"
    user.company = "Ozone"
    user.email = "marketing@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 1
    user.save
    
    #Jefe de oficina
    user = User.new
    user.first_name = "Helena"
    user.last_name = "Rivas"
    user.company = "Ozone"
    user.email = "hello@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 2
    user.save

    #Usuarios profesionales
    user = User.new
    user.first_name = "Daniela"
    user.last_name = "Brache"
    user.company = "Ozone"
    user.email = "daniela.brache@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 3
    user.save
  
    user = User.new
    user.first_name = "Melissa"
    user.last_name = "Visbal"
    user.company = "Ozone"
    user.email = "melissa.visbal@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 3
    user.save
  
    user = User.new
    user.first_name = "Pedro"
    user.last_name = "Consuegra"
    user.company = "Ozone"
    user.email = "software@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 3
    user.save

    user = User.new
    user.first_name = "Wendy"
    user.last_name = "Romero"
    user.company = "Ozone"
    user.email = "wendy.romero@ozonelabs.us"
    user.password = 12345678
    user.password_confirmation = 12345678 
    user.role_id = 3
    user.save
    
    
    Complaint.delete_all
    
    tipos_de_proceso_array = ["Proceso Ordinario","Proceso Verbal"]
    
    ordinario_etapas = Etapa.where("tipo = ?","ordinario")
    verbal_etapas = Etapa.where("tipo = ?","verbal")
    
    users = User.all
    
    for i in(1..40)
      
      complaint = Complaint.new
      complaint.id = i
      complaint.consecutivo = i
      complaint.tipo_de_proceso = "Proceso Ordinario"
      
      complaint.etapa_id = ordinario_etapas.sample.id
      
      complaint.hechos = Faker::Lorem.paragraph(sentence_count = 3)
      complaint.fecha_hechos = Time.now.to_formatted_s(:db)
      complaint.creador_id = 2
      complaint.save!
      
      aux = rand(1..3)
      
      if aux == 1 || aux == 2
        users_complaint = UsersComplaint.new
        users_complaint.user_id = users[aux].id
        users_complaint.complaint_id = complaint.id
        users_complaint.save
      end
      
    end
    
  end
end