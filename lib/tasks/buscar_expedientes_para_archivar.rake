# encoding: utf-8
namespace :admin  do
  desc "create some fake data"
  task :buscar_expedientes_para_archivar => :environment do
  
    complaints = Complaint.all
    for complaint in complaints
      complaint.archivar_expedientes    
    end

  end
end
