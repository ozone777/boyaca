# encoding: utf-8
namespace :admin  do
  desc "create some fake data"
  task :tipo_de_proceso => :environment do
  
    complaints = Complaint.all
    for complaint in complaints
        complaint.tipo_de_proceso = "PROCESO ORDINARIO"
        complaint.save
    end
    
  end
end
