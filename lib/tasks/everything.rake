namespace :admin do
   task :everything => [:initialize, :fake_data] do
     desc "Recreate everything from scratch including pre-populated data"
  end
end