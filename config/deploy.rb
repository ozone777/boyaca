task :send_file_to_dedicated, :roles => [:web] do

  #run "cd #{release_path} && rm -rf vendor/bundle" 
  run "cd #{release_path} && rm -rf vendor/bundle"
 
  run "ln -s /home/ozone/#{application}/shared/bundle #{release_path}/vendor/bundle"  
  
  run "rm -rf #{release_path}/config/database.yml"
  run "ln -s /home/ozone/#{application}/shared/database.yml #{release_path}/config/database.yml"   
                                       
  run "cd #{release_path} && bundle install --deployment"
  #run "cd #{release_path} && bundle exec rake db:drop RAILS_ENV=production"
  #run "cd #{release_path} && bundle exec rake db:create RAILS_ENV=production"
  run "cd #{release_path} && bundle exec rake db:migrate RAILS_ENV=production"
  #run "cd #{release_path} && bundle exec rake assets:precompile RAILS_ENV=production"
  run "cd #{release_path} && bundle exec rake assets:precompile --trace"

  
  
  run "chmod -R 755 /home/ozone"
  run "chmod -R 755 /home/ozone/#{application}"
  
  run "chown -R ozone:wheel #{release_path}"
  run "chown -R ozone:wheel #{shared_path}"
 # run "chown ozone:wheel /home/ozone/boyaca/current"
  

  run "/etc/init.d/nginx restart"
                                                                                     
end

task :send_to_boyaca_server, :roles => [:web] do

  #run "cd #{release_path} && rm -rf vendor/bundle" 
  run "cd #{release_path} && rm -rf vendor/bundle"
 
  run "ln -s /home/sozonegroup/boyaca/shared/bundle #{release_path}/vendor/bundle"  
  
  run "rm -rf #{release_path}/config/database.yml"
  run "ln -s /home/sozonegroup/boyaca/shared/database.yml #{release_path}/config/database.yml"   
                                       
  run "cd #{release_path} && bundle install --deployment"
  #run "cd #{release_path} && bundle exec rake db:drop RAILS_ENV=production"
  #run "cd #{release_path} && bundle exec rake db:create RAILS_ENV=production"
  run "cd #{release_path} && bundle exec rake db:migrate RAILS_ENV=production"
  #run "cd #{release_path} && bundle exec rake assets:precompile RAILS_ENV=production"
  run "cd #{release_path} && bundle exec rake assets:precompile --trace"

  
  
  run "chmod -R 755 /home/sozonegroup"
  run "chmod -R 755 /home/sozonegroup/#{application}"
  
  run "chown -R sozonegroup:sozonegroup #{release_path}"
  run "chown -R sozonegroup:sozonegroup #{shared_path}"
 # run "chown ozone:wheel /home/ozone/boyaca/current"
  

  run "/etc/init.d/nginx restart"
                                                                                     
end



#git clone -b master https://bitbucket.org/ozone777/boyaca.git

task :servidor_boyaca do
  ssh_options[:port] = 8787
  set :ip, "190.90.95.148" 
  set :user, "root" 
  #nombre del dominio
  set :server_name, "201.245.195.2"
  #La direccion en donde el repositorio
  set :domain, "ozone.sourcerepo.com" 
  #nombre de la aplicacion
  set :application, "boyaca"
  set :default_shell, '/bin/bash -l'   # fix error [err :: ozonelabsserver1.com] stdin: is not a tty 
  set :scm, :git 
  #Nombre del repositorio
  set :repository, "https://bitbucket.org/ozone777/boyaca.git" 
  #Donde yo quiero que lo grabe en linux
  set :deploy_to, "/home/sozonegroup/#{application}" 
  set :use_sudo, false 
  set :group_writable, false 
  set :branch, 'master' 
  set :deploy_via, :checkout 
  default_run_options[:pty] = true 
  set :rails_env, "production" 
  #role :app, server_name 
  role :web, server_name 
  #role :db, server_name, :primary => true 
end

task :boyaca do
  ssh_options[:port] = 13727
  set :ip, "45.56.123.86" 
  set :user, "root" 
  #nombre del dominio
  set :server_name, "45.56.123.86"
  #La direccion en donde el repositorio
  set :domain, "ozone.sourcerepo.com" 
  #nombre de la aplicacion
  set :application, "boyaca"
  set :default_shell, '/bin/bash -l'   # fix error [err :: ozonelabsserver1.com] stdin: is not a tty 
  set :scm, :git 
  #Nombre del repositorio
  set :repository, "git@bitbucket.org:ozone777/boyaca.git" 
  #Donde yo quiero que lo grabe en linux
  set :deploy_to, "/home/ozone/#{application}" 
  set :use_sudo, false 
  set :group_writable, false 
  set :branch, 'master' 
  set :deploy_via, :checkout 
  default_run_options[:pty] = true 
  set :rails_env, "production" 
  #role :app, server_name 
  role :web, server_name 
  #role :db, server_name, :primary => true 
end


#Importante
#La primera vez que vayas a hacer el deploy

#after :servidor_boyaca, 'deploy:setup'
#after :boyaca, 'deploy:setup'
#after :linode, 'deploy:setup'
#after :briefmachine, :deploy
#after :teachstars_international, :deploy


#after 'deploy:finalize_update', :send_file_to_dedicated 
after 'deploy:finalize_update', :send_to_boyaca_server 


after :servidor_boyaca, :deploy
after :boyaca, :deploy
