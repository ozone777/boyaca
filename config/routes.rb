BriefMachine::Application.routes.draw do

  resources :gob_docs
  resources :gob_vars
  resources :ofiles

  resources :complaints do 
    collection do
      get :search
      post :save_assign
      get :assign
      get :mycomplaints
      get :en_tramite
      get :no_asignadas
      get :procedimiento_ordinario
      get :procedimiento_verbal
      get :reabrir_expediente
    end
  end
  
  
  get '/edit_my_account', :to => 'boyaca#edit_my_account'
  get '/ayuda', :to => 'boyaca#ayuda'
  put '/update_my_account', :to => 'boyaca#update_my_account'
  
  
  get '/en_tramite', :to => 'complaints#en_tramite'
  get '/no_asignadas', :to => 'complaints#no_asignadas'
  get '/procedimiento_ordinario', :to => 'complaints#procedimiento_ordinario'
  get '/procedimiento_verbal', :to => 'complaints#procedimiento_verbal'
  get '/estadisticas', :to => 'complaints#estadisticas'
  get '/expedientes_archivados', :to => 'complaints#expedientes_archivados'
  #get '/reabrir_expediente', :to => 'complaints#reabrir_expediente'
  
  get '/admin/users', :to => 'admin#users', :as => :admin_users
  get '/admin/users/:user_id', :to => 'admin#show_user', :as => :admin_show_user
  get '/admin/edit_user/:user_id', :to => 'admin#edit_user', :as => :admin_edit_user
  put '/admin/edit_user/:user_id', to: 'admin#update_user'
  delete '/admin/destroy_user/:user_id', :to => 'admin#destroy_user', :as => :admin_destroy_user
  get '/admin/new_user', :to => 'admin#new_user', :as => :admin_new_user
  post '/admin/new_user', to: 'admin#create_user'
  post '/save_assign', to: 'admin#save_assign', :as => :save_assign

  devise_for :users, :controllers => { :registrations => 'registrations' }
  devise_for :users
      

  #root :to => 'complaints#index'
  #root :to => 'landing#landing_index'

  devise_scope :user do
    #root :to => 'devise/sessions#new'
    root :to => 'complaints#index'
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.


  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
