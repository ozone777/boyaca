class ComplaintsController < ApplicationController
  # GET /complaints
  # GET /complaints.json
  
  load_and_authorize_resource
  
  before_filter :set_semaforo, :only => [:index, :en_tramite, :no_asignadas, :procedimiento_ordinario, :procedimiento_verbal, :search]
  
  #from posts p inner join groups_posts gp on p.id = gp.post_id
  #        inner join groups g on g.id = gp.group_id
  #        inner join group_memberships gm on gm.group_id = g.id
  #        inner join users u on u.id = gm.user_id 
  #        left join tutor_languages tl on tl.id = u.tutor_language_id
  #        left join group_docs gd on g.profile_pic = gd.id 
  
  
  def set_semaforo
		@semaforo = Hash.new
		@semaforo["rojo"] =     GobVar.meta("semaforo_rojo")
		@semaforo["amarillo"] = GobVar.meta("semaforo_amarillo")
	  @semaforo["verde"] =    GobVar.meta("semaforo_verde")
  end
  
  def index
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    
		@semaforo = Hash.new
		@semaforo["rojo"] =     GobVar.meta("semaforo_rojo")
		@semaforo["amarillo"] = GobVar.meta("semaforo_amarillo")
	  @semaforo["verde"] =    GobVar.meta("semaforo_verde")
    
    ######
    
    if current_user.profesional?
      sql = "select c.nombre_de_abogado_asignado, c.archivado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email, c.anulada , c.ultima_actuacion, e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
      left join users u on u.id = uc.user_id
      left join etapas e on e.id = c.etapa_id
      where uc.user_id = #{current_user.id} and c.anulada is not true and c.archivado is true ORDER BY c.consecutivo DESC"
      
      @title = "Expedientes asignados"
      
    elsif current_user.aux_administrativo? || current_user.admin? || current_user.jefe_oficina?
      sql = "select c.nombre_de_abogado_asignado, c.archivado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email , c.anulada , c.ultima_actuacion, e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c left join users_complaints uc on c.id = uc.complaint_id 
      left join etapas e on e.id = c.etapa_id
      left join users u on u.id = uc.user_id where c.anulada is not true and c.archivado is not true
      ORDER BY c.consecutivo DESC"
      
      @title = "Todos los expedientes"
      
    end
    
    @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
  end
  
  def expedientes_archivados
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    
		@semaforo = Hash.new
		@semaforo["rojo"] =     GobVar.meta("semaforo_rojo")
		@semaforo["amarillo"] = GobVar.meta("semaforo_amarillo")
	  @semaforo["verde"] =    GobVar.meta("semaforo_verde")
    
    @title = "Expedientes archivados"
    
    ######
    
     if current_user.profesional?
          sql = "select c.nombre_de_abogado_asignado, c.archivado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email, c.anulada , c.ultima_actuacion, e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
          left join users u on u.id = uc.user_id
          left join etapas e on e.id = c.etapa_id
          where uc.user_id = #{current_user.id} and c.anulada is not true and c.archivado is true ORDER BY c.consecutivo DESC"
        elsif current_user.aux_administrativo? || current_user.admin? || current_user.jefe_oficina?
          sql = "select c.nombre_de_abogado_asignado, c.archivado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email , c.anulada , c.ultima_actuacion, e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c left join users_complaints uc on c.id = uc.complaint_id 
          left join etapas e on e.id = c.etapa_id
          left join users u on u.id = uc.user_id where c.anulada is not true and c.archivado is true
          ORDER BY c.consecutivo DESC"
        end
    
        @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    
        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @complaints }
        end
    
  end
  
  
  def en_tramite
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    ######
    
    @title = "Expedientes en trámite"
    
    sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.fecha_hechos, c.anulada, c.tipo_de_proceso, c.id, u.email, e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name  from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
    left join etapas e on e.id = c.etapa_id
    left join users u on u.id = uc.user_id where c.anulada is not true
    ORDER BY c.consecutivo DESC"
    
    @countComplaints = Complaint.count
    @quejas_en_tramite = Complaint.find_by_sql(sql).length
    @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
    
  end
  
  
 
  def no_asignadas
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    ######
    
    @title = "Expedientes no asignados"
    
    sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.anulada, c.id, u.email , e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c left join users_complaints uc on c.id = uc.complaint_id 
    left join etapas e on e.id = c.etapa_id
    left join users u on u.id = uc.user_id where email IS NULL and c.anulada is not true
    ORDER BY c.consecutivo DESC"
    
    @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
  end
  
  
  def procedimiento_ordinario
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    ######
    
    @title = "Expedientes tipo procedimiento ordinario"
    
    sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email, c.anulada , e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c left join users_complaints uc on c.id = uc.complaint_id 
    left join etapas e on e.id = c.etapa_id
    left join users u on u.id = uc.user_id where tipo_de_proceso = 'Proceso Ordinario' and c.anulada is not true
    ORDER BY c.consecutivo DESC"
    
    @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
  end
  
  
  def procedimiento_verbal
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    ######
    
    @title = "Expedientes tipo procedimiento verbal"
    
    sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string,  c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.fecha_hechos, c.tipo_de_proceso, c.id, u.email, c.anulada , e.name, c.etapa_del_expediente, c.consecutivo, u.first_name, u.last_name from complaints c left join users_complaints uc on c.id = uc.complaint_id 
    left join etapas e on e.id = c.etapa_id
    left join users u on u.id = uc.user_id where tipo_de_proceso = 'Proceso Verbal' and c.anulada is not true
    ORDER BY c.consecutivo DESC"
    
    @complaints = Complaint.paginate_by_sql(sql, :page => params[:page], :page => params[:page], :per_page => 20)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
  end

  
  
  
  def search
    
    #Estadisticas
    
    @quejas_en_tramite = Complaint.find_by_sql(Complaint.en_tramite).length
    @numero_de_quejas = Complaint.count
    @quejas_no_asignadas = @numero_de_quejas - @quejas_en_tramite
    @procedimient_verbal = Complaint.where("tipo_de_proceso = ?","Proceso Verbal").count
    @procedimient_ordinario = Complaint.where("tipo_de_proceso = ?","Proceso Ordinario").count
    @expedientes_archivados = Complaint.where("archivado = ?",true).count
    ######
    
    @title = "Busqueda"
    
    if current_user.profesional?
      sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion,  c.consecutivo, c.fecha_hechos, c.tipo_de_proceso, c.anulada , e.name , c.id, u.first_name, u.last_name , c.etapa_del_expediente  from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
      left join etapas e on e.id = c.etapa_id
      left join users u on u.id = uc.user_id
      where uc.user_id = #{current_user.id} and c.anulada is not true and (c.hechos LIKE '%#{params[:data]}%' OR c.campo_de_la_ultima_actuacion LIKE '%#{params[:data]}%' OR c.ano LIKE '%#{params[:data]}%' OR c.consecutivo LIKE '%#{params[:data]}%' OR e.name LIKE '%#{params[:data]}%' OR u.first_name LIKE '%#{params[:data]}%' OR u.last_name LIKE '%#{params[:data]}%' ) 
      ORDER BY c.consecutivo DESC "
    elsif current_user.aux_administrativo?
      sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.consecutivo, c.fecha_hechos, c.tipo_de_proceso, c.id, u.first_name, u.last_name ,  c.anulada , e.name, u.email , c.etapa_del_expediente from complaints c left join users_complaints uc on c.id = uc.complaint_id 
      left join etapas e on e.id = c.etapa_id
      left join users u on u.id = uc.user_id where c.anulada is not true and (c.hechos LIKE '%#{params[:data]}%' OR c.campo_de_la_ultima_actuacion LIKE '%#{params[:data]}%' OR c.ano LIKE '%#{params[:data]}%' OR c.consecutivo LIKE '%#{params[:data]}%' OR  e.name LIKE '%#{params[:data]}%' OR u.first_name LIKE '%#{params[:data]}%' OR u.last_name LIKE '%#{params[:data]}%') 
      ORDER BY c.consecutivo DESC"
    elsif current_user.admin? || current_user.jefe_oficina?
      sql = "select c.nombre_de_abogado_asignado, c.ordinario_apertura_fecha_actuacion, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite, c.campo_de_la_ultima_actuacion, c.ultima_actuacion, c.consecutivo, c.fecha_hechos, c.tipo_de_proceso, c.id, u.first_name, u.last_name , u.email , c.anulada , e.name, c.etapa_del_expediente from complaints c left join users_complaints uc on c.id = uc.complaint_id 
      left join etapas e on e.id = c.etapa_id
      left join users u on u.id = uc.user_id where c.anulada is not true and (c.hechos LIKE '%#{params[:data]}%' OR c.campo_de_la_ultima_actuacion LIKE '%#{params[:data]}%' OR c.ano LIKE '%#{params[:data]}%' OR  c.consecutivo LIKE '%#{params[:data]}%' OR e.name LIKE '%#{params[:data]}%' OR u.first_name LIKE '%#{params[:data]}%' OR u.last_name LIKE '%#{params[:data]}%') 
      ORDER BY c.consecutivo DESC"
    end
    
    @complaints = Complaint.paginate_by_sql(sql,:page => params[:page], :page => params[:page], :per_page => 20)
    
    @countComplaints = Complaint.count
    
    sqltramite = "select c.nombre_de_abogado_asignado, c.ano, c.etapa_id, c.fecha_queja_string, c.fecha_recibido_en_esta_dependencia_string, c.fecha_hechos_string, c.fecha_queja, c.fecha_recibido_en_esta_dependencia, c.origen_de_queja, c.disciplinado, c.cedula_disciplinado, c.entidad, c.municipio, c.fecha_limite,  c.fecha_hechos, c.tipo_de_proceso, c.anulada, c.id, u.email  from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
    left join users u on u.id = uc.user_id where c.anulada is not true"
    
    @quejas_en_tramite = Complaint.find_by_sql(sqltramite).length
    @params = params[:data]
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @complaints }
    end
  end
  
  def reabrir_expediente
    
    @complaint = Complaint.find(params[:expediente_id])
    @complaint.archivado = false
      
    @complaint.ordinario_indagacion_terminacion_archivo_del_expediente = false
    @complaint.ordinario_apertura_terminacion_archivo_del_expediente = false
    @complaint.ordinario_prorroga_terminacion_archivo_del_expediente = false
    @complaint.ordinario_cierre_terminacion_archivo_del_expediente = false
    @complaint.ordinario_descargos_terminacion_archivo_del_expediente = false
    @complaint.ordinario_descargos_terminacion_archivo_del_expediente = false
    
    @complaint.save
    
    redirect_to "/expedientes_archivados"
    
  end
  
  
  def assign
    @complaint = Complaint.find(params[:cid])
    @users_assigned = UsersComplaint.where("complaint_id = ?", params[:cid])
    # Profesionales
    @users = User.where("role_id = ?", 3)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @complaint }
    end
  end

  def mycomplaints
    if current_user.role_id == 3
      sql = "select c.* from complaints c inner join users_complaints uc on c.id = uc.complaint_id where uc.user_id = #{current_user.id}"
      @mycomplaints = Complaint.find_by_sql(sql)
    end
  end

  def autoterminacionyarchivo
    @complaint = Complaint.find(params[:complaint_id])
  end

  def archivar
    @complaint = Complaint.find(params[:complaint_id])
    @complaint.update_attribute(:estado_proceso, 3)
    respond_to do |format|
      format.html { redirect_to mycomplaints_complaints_path, notice: 'Queja archivada.' }
    end
  end
  # GET /complaints/1
  # GET /complaints/1.json
  def show
    @ofile = Ofile.new
    @complaint = Complaint.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @complaint }
    end
  end

  # GET /complaints/new
  # GET /complaints/new.json
  def new
    @complaint = Complaint.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @complaint }
    end
  end

  # GET /complaints/1/edit
  def edit
    @ofile = Ofile.new
    
    @complaint = Complaint.find(params[:id])
    @ofiles = @complaint.ofiles.order("avatar_content_type DESC")
    @complaints_anuladas = Complaint.where("parent_id = ?",@complaint.id)
    
    if @complaint.tipo_de_proceso == "PROCESO ORDINARIO"
      @etapas = Etapa.where("tipo = ?","ordinario")
    else
      @etapas = Etapa.where("tipo = ?","verbal")
    end
    
  end

  # POST /complaints
  # POST /complaints.json
  def create
    @complaint = Complaint.new(params[:complaint])

    respond_to do |format|
      if @complaint.save
        format.html { redirect_to edit_complaint_path(@complaint), message: 'Queja creada exitosamente' }
        format.json { render json: @complaint, status: :created, location: @complaint }
      else
        format.html { redirect_to edit_complaint_path(@complaint), message: 'Error de creación de queja' }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /complaints/1
  # PUT /complaints/1.json
  def update
    
    @complaint = Complaint.find(params[:id])
    #@complaint.step = params[:step]

    #DateTime.strptime(time_string, "%m/%d/%Y %I:%M %P")
    
    respond_to do |format|
      if @complaint.update_attributes(params[:complaint])
        format.html { redirect_to :back, notice: 'Queja actualizada correctamente.' }
        format.json { head :no_content }
      else
        format.html { redirect_to :back, notice: 'Error en actualizacion'  }
        format.json { render json: @ofile.errors, status: :unprocessable_entity }
      end
    end
    
    
  end

  # DELETE /complaints/1
  # DELETE /complaints/1.json
  def destroy
    @complaint = Complaint.find(params[:id])
    @complaint.destroy

    respond_to do |format|
      format.html { redirect_to complaints_url }
      format.json { head :no_content }
    end
  end
  
  def save_assign
    @usersComplaint = UsersComplaint.where("complaint_id = ?", params[:complaint_id]).first
    
    
    if @usersComplaint
      puts "Reasignar Queja"
      #Reasignar Queja
      
      @usersComplaint.user_id = params[:user_id]
     # @usersComplaint.jefe_oficina_id = current_user.id
      @usersComplaint.save
      
      @user = @usersComplaint.user
      @user.message = "Queja reasignada."

    else
      puts "Nueva Asignacion"
      #Nueva Asignacion
      
      @usersComplaint = UsersComplaint.new
      @usersComplaint.user_id = params[:user_id]
      @usersComplaint.complaint_id = params[:complaint_id]
      #@usersComplaint.jefe_oficina_id = current_user.id
      
      @usersComplaint.save!
      
      @user = @usersComplaint.user
      @user.message =  "Queja asignada exitosamente"
     
      puts "User id"
      puts @user.id
     
    end
    
    respond_to do |format|
      format.json { render json: @user.to_json }
    end
    
  end
  
end
