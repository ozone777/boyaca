class BoyacaController < ApplicationController
  # GET /ofiles
  # GET /ofiles.json

  before_filter :authenticate_user!
  
  def update_my_account
    
    @user = User.find(current_user.id)

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_path, notice: 'Usuario actualizado exitosamente.' }
      else
        format.html { redirect_to "/" }
      end
    end
    
  end
  
  
  def edit_my_account
    
    @user = User.find(current_user.id)
  end
  
  
end
