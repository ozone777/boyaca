class GobVarsController < ApplicationController
  # GET /gob_vars
  # GET /gob_vars.json
  def index
    @gob_vars = GobVar.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gob_vars }
    end
  end

  # GET /gob_vars/1
  # GET /gob_vars/1.json
  def show
    @gob_var = GobVar.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gob_var }
    end
  end

  # GET /gob_vars/new
  # GET /gob_vars/new.json
  def new
    @gob_var = GobVar.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gob_var }
    end
  end

  # GET /gob_vars/1/edit
  def edit
    @gob_var = GobVar.find(params[:id])
  end

  # POST /gob_vars
  # POST /gob_vars.json
  def create
    @gob_var = GobVar.new
    
    if params[:meta_value_color] && params[:meta_value_color] != ""
      @gob_var.meta_value = params[:meta_value_color]
    else
      @gob_var.meta_value = params[:gob_var][:meta_value]
    end
    @gob_var.meta_key = params[:gob_var][:meta_key]
    
    respond_to do |format|
      if @gob_var.save
        format.html { redirect_to gob_vars_url, notice: 'Gob var was successfully created.' }
        format.json { render json: @gob_var, status: :created, location: @gob_var }
      else
        format.html { render action: "new" }
        format.json { render json: @gob_var.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gob_vars/1
  # PUT /gob_vars/1.json
  def update
    @gob_var = GobVar.find(params[:id])
    
    if params[:meta_value_color] && params[:meta_value_color] != ""
      @gob_var.meta_value = params[:meta_value_color]
    else
      @gob_var.meta_value = params[:gob_var][:meta_value]
    end
    @gob_var.meta_key = params[:gob_var][:meta_key]
    
    respond_to do |format|
      if @gob_var.save
        format.html { redirect_to gob_vars_url, notice: 'Gob var was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gob_var.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gob_vars/1
  # DELETE /gob_vars/1.json
  def destroy
    @gob_var = GobVar.find(params[:id])
    @gob_var.destroy

    respond_to do |format|
      format.html { redirect_to gob_vars_url }
      format.json { head :no_content }
    end
  end
end
