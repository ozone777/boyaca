class AdminController < ApplicationController
  
  before_filter :need_to_be_admin
  
  def users
    sql = "select * from users where id != #{current_user.id}"
    #@users = User.find_by_sql(sql)
    @users = User.all
  end

  def show_user
    @user = User.find(params[:user_id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new_user
    @user = User.new
    @roles = Role.all
  end
  
  def create_user
    @roles = Role.all
    @user = User.new(params[:user])
    respond_to do |format|
      if @user.save 
        format.html { redirect_to complaints_path }
      else
        format.html { redirect_to :back }
      end
    end
  end
  
  def edit_user
    @user = User.find(params[:user_id])
    @roles = Role.all
  end

  def update_user
    @user = User.find(params[:user_id])
    @roles = Role.all

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_path, notice: 'Usuario actualizado exitosamente.' }
      else
        format.html { render action: "edit_user" }
      end
    end
  end

  def destroy_user
    @user = User.find(params[:user_id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to admin_users_path }
      format.json { head :no_content }
    end
  end

  
  
end
