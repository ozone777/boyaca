class GobDocsController < ApplicationController
  # GET /gob_docs
  # GET /gob_docs.json
  def index
    @gob_docs = GobDoc.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gob_docs }
    end
  end

  # GET /gob_docs/1
  # GET /gob_docs/1.json
  def show
    @gob_doc = GobDoc.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gob_doc }
    end
  end

  # GET /gob_docs/new
  # GET /gob_docs/new.json
  def new
    @gob_doc = GobDoc.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gob_doc }
    end
  end

  # GET /gob_docs/1/edit
  def edit
    @gob_doc = GobDoc.find(params[:id])
  end

  # POST /gob_docs
  # POST /gob_docs.json
  def create
    @gob_doc = GobDoc.new(params[:gob_doc])
    @gob_doc.dpto = params[:dpto]
    
    respond_to do |format|
      if @gob_doc.save
        if @gob_doc.dpto == "control"
          @gob_doc.process_doc
        elsif @gob_doc.dpto == "Usuarios"
          @gob_doc.create_users
        end
        format.html { redirect_to gob_docs_url, notice: 'Gob doc was successfully created.' }
        format.json { render json: @gob_doc, status: :created, location: @gob_doc }
      else
        format.html { render action: "new" }
        format.json { render json: @gob_doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gob_docs/1
  # PUT /gob_docs/1.json
  def update
    @gob_doc = GobDoc.find(params[:id])

    respond_to do |format|
      if @gob_doc.update_attributes(params[:gob_doc])
        format.html { redirect_to @gob_doc, notice: 'Gob doc was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gob_doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gob_docs/1
  # DELETE /gob_docs/1.json
  def destroy
    @gob_doc = GobDoc.find(params[:id])
    @gob_doc.destroy

    respond_to do |format|
      format.html { redirect_to gob_docs_url }
      format.json { head :no_content }
    end
  end
end
