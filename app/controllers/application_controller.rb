class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :authenticate_user!
  before_filter :set_locale
  
  rescue_from CanCan::AccessDenied do |exception|
      redirect_to "/complaints", :alert => exception.message
  end
  
  def set_locale
    I18n.locale = "es"
  end
  
  def need_to_be_admin
    
    if current_user
      if current_user.role_id = 5
        true
      else
        false
      end
    else
      false
    end    
    
  end
  
  
end
