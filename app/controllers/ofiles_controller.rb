class OfilesController < ApplicationController
  # GET /ofiles
  # GET /ofiles.json
  def index
    @ofiles = Ofile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ofiles }
    end
  end

  # GET /ofiles/1
  # GET /ofiles/1.json
  def show
    @ofile = Ofile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ofile }
    end
  end

  # GET /ofiles/new
  # GET /ofiles/new.json
  def new
    @ofile = Ofile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ofile }
    end
  end

  # GET /ofiles/1/edit
  def edit
    @ofile = Ofile.find(params[:id])
  end

  # POST /ofiles
  # POST /ofiles.json
  def create
    @ofile = Ofile.new(params[:ofile])
    @complaint = @ofile.complaint
    
    puts "ACA STEPS"
    puts params[:step]
    
    
    if params[:step] == "default-title-0"
      @ofile.step = "step0"
      @complaint.step = "step0"
    elsif params[:step] == "default-title-1"
      @ofile.step = "step1"
      @complaint.step = "step1"
    elsif params[:step] == "default-title-2"
      @ofile.step = "step2"
      @complaint.step = "step2"
    elsif params[:step] == "default-title-3"
      @ofile.step = "step3"
      @complaint.step = "step3"
    elsif params[:step] == "default-title-4"
      @ofile.step = "step4"
      @complaint.step = "step4"
    elsif params[:step] == "default-title-5"
      @ofile.step = "step5"
      @complaint.step = "step5"
    elsif params[:step] == "default-title-6"
      @ofile.step = "step6"
      @complaint.step = "step6"
    elsif params[:step] == "default-title-7"
      @ofile.step = "step7"
      @complaint.step = "step7"
    elsif params[:step] == "default-title-8"
      @ofile.step = "step8"
      @complaint.step = "step8"
    elsif params[:step] == "default-title-9"
      @ofile.step = "step9"
      @complaint.step = "step9"
    elsif params[:step] == "default-title-10"
      @ofile.step = "step10"
      @complaint.step = "step10"
    end
    
    @complaint.save
    
    respond_to do |format|
      if @ofile.save
        format.html { redirect_to edit_complaint_path(@complaint), notice: 'Archivo añadido exitosamente' }
        format.json { render json: @complaint, status: :created, location: @complaint }
      else
        format.html { redirect_to edit_complaint_path(@complaint), notice: 'Por favor seleccione un archivo valido' }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ofiles/1
  # PUT /ofiles/1.json
  def update
    @ofile = Ofile.find(params[:id])
    
    
    
    respond_to do |format|
      if @ofile.update_attributes(params[:ofile])
        format.html { redirect_to @ofile, notice: 'Ofile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ofile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ofiles/1
  # DELETE /ofiles/1.json
  def destroy
    @ofile = Ofile.find(params[:id])
    @ofile.destroy

    respond_to do |format|
      format.html { redirect_to ofiles_url }
      format.json { head :no_content }
    end
  end
end
