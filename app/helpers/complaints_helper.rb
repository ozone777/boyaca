module ComplaintsHelper
  
  def semaforo_css_pro(option,value,semaforo)
    
    verde = semaforo["verde"]
    amarillo = semaforo["amarillo"]
    rojo = semaforo["rojo"]
    
    if value
    
    if option == "tramite"
     
        if value > 1 && value <= 5
          return "<div class = 'semaforo' style= 'color:#{verde}'>#{value}</div>"
        elsif value > 5 && value <=10
          return "<div class = 'semaforo' style= 'color:#{amarillo}'>#{value}</div>"
        elsif value > 10
          return "<div class = 'semaforo' style= 'color:#{rojo}'>#{value}</div>"  
        end
        
    elsif option == "caducidad" || option == "prescripcion"

      if value <  730
        return "<div class = 'semaforo' style= 'color:#{rojo}'>#{value}</div>"
      elsif value > 730 && value <=1095
        return "<div class = 'semaforo' style= 'color:#{amarillo}'>#{value}</div>"
      elsif value > 1095
        return "<div class = 'semaforo' style= 'color:#{verde}'>#{value}</div>"  
      end
      
    end
    
    end
    ""
  end
  
  
  def obtener_nombre_de_ultima_actuacion(field)
    
    nombres_de_actuaciones = Hash[
                    "ordinario_evaluacion_fecha_actuacion" => "Evaluación de la noticia disciplinaria", 
                    "ordinario_evaluacion_nulidad_fecha_actuacion" => "Nulidad de la evaluación de la noticia disciplinaria",
                    "ordinario_indagacion_fecha_actuacion" => "Idagación preliminar",
                    "ordinario_indagacion_terminacion_fecha_actuacion" => "Archivo de la indagación preliminar", 
                    "ordinario_indagacion_nulidad_fecha_actuacion" => "Nulidad de indagación preliminar",
                    "ordinario_apertura_fecha_actuacion" => "Apertura de la investigación disciplinaria",
                    "ordinario_apertura_terminacion_fecha_actuacion" => "Archivo de apertura de la investigación disciplinaria", 
                    "ordinario_apertura_nulidad_fecha_actuacion" => "Nulidad de apertura de la investigación disciplinaria",
                    "ordinario_prorroga_fecha_actuacion" => "Prorroga de la investigación disciplinaria",
                    "ordinario_prorroga_terminacion_fecha_actuacion" => "Archivo de prorroga de la investigación disciplinaria", 
                    "ordinario_prorroga_nulidad_fecha_actuacion" => "Nulidad de prorroga de la investigación disciplinaria",
                    "ordinario_cierre_fecha_actuacion" => "Cierre de la investigación",
                    "ordinario_cierre_terminacion_fecha_actuacion" => "Archivo de cierre de la investigación", 
                    "ordinario_cierre_nulidad_fecha_actuacion" => "Nulidad de cierre de la investigación",
                    "ordinario_pliego_de_cargos_fecha_actuacion" => "Pliego de cargos",
                    "ordinario_pliego_de_cargos_segunda_fecha_actuacion" => "Pliego de cargos segunda fecha de actuación", 
                    "ordinario_pliego_de_cargos_nulidad_fecha_actuacion" => "Nulidad de pliego de cargos",
                    "ordinario_descargos_fecha_actuacion" => "Descargos",
                    "ordinario_descargos_recurso_de_apelacion_fecha_actuacion" => "Descargos recurso de apelación", 
                    "ordinario_descargos_terminacion_fecha_actuacion" => "Archivo de descargos",
                    "ordinario_descargos_nulidad_fecha_actuacion" => "Nulidad de descargos",
                    "ordinario_alegatos_terminacion_fecha_actuacion" => "Alegatos de conclusión", 
                    "ordinario_alegatos_nulidad_fecha_actuacion" => "Nulidad de alegatos de conclusión",
                    "ordinario_fallo_en_primera_fecha_actuacion" => "Fallo en primera instancia",
                    "ordinario_fallo_en_primera_nulidad_fecha_actuacion" => "Nulidad fallo en primera instancia", 
                    "ordinario_fallo_en_segunda_fecha_actuacion" => "Fallo de segunda instancia",
                    "ordinario_fallo_en_segunda_nulidad_fecha_actuacion" => "Nulidad de fallo de segunda instancia",
                    "ordinario_indagacion_terminacion_archivo_confirma_actuacion" => "Confirmación de archivo de indagación preliminar", 
                    "ordinario_apertura_terminacion_archivo_confirma_actuacion" => "Confirmación de archivo de apertura de la investigación disciplinaria",
                    "ordinario_prorroga_terminacion_archivo_confirma_actuacion" => "Confirmación de archivo de prorroga de la investigación disciplinaria",
                    "ordinario_cierre_terminacion_archivo_confirma_actuacion" => "Confirmacion de archivo cierre de la investigación", 
                    "ordinario_descargos_terminacion_archivo_confirma_actuacion" => "Confirmación de archivo de descargos",
                    "ordinario_alegatos_terminacion_archivo_confirma_actuacion" => "Confirmación de archivo de alegatos de conclusión",
                    "ordinario_evaluacion_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de la evaluación de la noticia disciplinaria", 
                    "ordinario_indagacion_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de indagación preliminar",
                    "ordinario_apertura_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de apertura de la investigación disciplinaria",
                    "ordinario_prorroga_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de prórroga de la investigación disciplinaria ", 
                    "ordinario_cierre_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de cierre de la investigación disciplinaria",
                    "ordinario_pliego_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de pliego de cargos",
                    "ordinario_descargos_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de descargos", 
                    "ordinario_alegatos_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de alegatos de conclusión",
                    "ordinario_fallo_en_primera_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de fallo en primera instancia",
                    "ordinario_fallo_en_segunda_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de fallo en segunda instancia", 
                    "ordinario_fallo_en_primera_recurso_fecha_actuacion" => "Fallo en primera recurso de apelación",
                    "ordinario_fallo_en_primera_revocatoria_fecha_actuacion" => "Revocatoria directa de fallo en primera instancia", 
                    "ordinario_indagacion_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de idagación preliminar",
                    "ordinario_apertura_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de apertura de la investigación disciplinaria", 
                    "ordinario_prorroga_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de prorroga de la investigación disciplinaria",
                    "ordinario_cierre_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de cierre de la investigación disciplinaria",
                    "ordinario_descargos_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de descargos", 
                    "ordinario_alegatos_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de alegatos de conclusión",
                    "ordinario_indagacion_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para indagación preliminar",
                    "ordinario_apertura_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para apertura de la investigación disciplinaria",
                    "ordinario_prorroga_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para prorroga de la investigación disciplinaria",
                    "ordinario_cierre_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para cierre de la investigación",
                    "ordinario_descargos_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para descargos",
                    "ordinario_alegatos_terminacion_fecha_actuacion_ap" => "Recurso de apelación de archivo para alegatos de conclusión",
                    
                    "verbal_citacion_fecha_actuacion" => "Citación a audiencia",
                    "verbal_citacion_nulidad_fecha_actuacion" => "Nulidad de citación a audiencia",
                    "verbal_primera_fecha_actuacion" => "Primeta audiencia", 
                    "verbal_primera_nulidad_fecha_actuacion" => "Nulidad de primera audiencia",
                    "verbal_segunda_fecha_actuacion" => "Segunda audiencia", 
                    "verbal_segunda_apelacion_actuacion" => "Apelación de degunda audiencia",
                    "verbal_segunda_nulidad_fecha_actuacion" => "Nulidad de segunda audiencia",
                    "verbal_citacion_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de citación a audiencia", 
                    "verbal_primera_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de primera audiencia",
                    "verbal_primera_alegatos_fecha_actuacion" => "Primera audiencia alegatos de conclusión",
                    "verbal_segunda_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de segunda audiencia", 
                    "verbal_decreto_de_pruebas_fecha_actuacion" => "Decreto de pruebas",
                    "verbal_evaluacion_fecha_actuacion" => "Evaluación de la noticia disciplinaria",
                    "verbal_evaluacion_nulidad_fecha_actuacion" => "Nulidad de evaluación de la npticia disciplinaria", 
                    "verbal_evaluacion_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de evaluación de la noticia disciplinaria",
                    "verbal_indagacion_fecha_actuacion" => "Indagación preliminar",
                    "verbal_indagacion_terminacion_fecha_actuacion" => "Archivo de indgación preliminar", 
                    "verbal_indagacion_nulidad_fecha_actuacion" => "Nulidad de indagación preliminar",
                    "verbal_indagacion_terminacion_archivo_confirma_actuacion" => "Confirmacion de archivo indagación preliminar",
                    "verbal_indagacion_nulidad_recurso_actuacion" => "Recurso de apelación para nulidad de indgación preliminar",
                    "verbal_indagacion_revocatoria_directa_fecha_actuacion" => "Revocatoria directa indgación preliminar",
                    "verbal_apertura_terminacion_fecha_actuacion" => "Archivo apertura de la investigación disciplinaria",
                    "verbal_apertura_nulidad_fecha_actuacion" => "Nulidad de la apertura de la investaigación disciplinaria",
                    "verbal_apertura_terminacion_archivo_confirma_actuacion" => "Confirma archivo apertura de la investigación disciplinaria",
                    "verbal_apertura_nulidad_recurso_actuacion" => "Recurso de apelación de nulidad de la apertura de la investigación disciplinaria",
                    "verbal_alegatos_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de alegatos de conclusión",
                    "verbal_prorroga_fecha_actuacion" => "Prorroga de la investigación disciplinaria",
                    "verbal_prorroga_terminacion_fecha_actuacion" => "Archivo de prorroga de la investigación disciplinaria",
                    "verbal_prorroga_nulidad_fecha_actuacion" => "Nulidad de prorroga de la investigación de disciplinaria ",
                    "verbal_prorroga_terminacion_archivo_confirma_actuacion" => "Confirma archivo de prorroga de la investigación disciplinaria",
                    "verbal_prorroga_nulidad_recurso_actuacion" => "Nulidad de prorroga de la investigación disciplinaria",
                    "verbal_prorroga_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de prorroga de la investigación disciplinaria",
                    "verbal_apertura_revocatoria_directa_fecha_actuacion" => "Revocatoria directa de aperura de la investigación",
                    
    ]
    
    return nombres_de_actuaciones[field]
    
  end
  
  
end
