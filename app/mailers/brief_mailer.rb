class BriefMailer < ActionMailer::Base
  default from: "we@briefmachine.com"

  def brief_created_confirmation(user, brief)
    @url = "http://briefmachine.railsplayground.net/briefs/#{brief.id}/answer"
  	mail(:to => user.email, :subject => "Brief creado")
  end

end