class Complaint < ActiveRecord::Base
  
  #Tutorial Adicionar, Modificar adicionar un campo
  #Se adiciona el campo en Etapas
  #Si es una actuacion adicionaralo en complaints_helper obtener_nombre_de_ultima_actuacion para traduccion del campo
  #Agregar al formulario donde corresponde
  
  attr_accessible :auto_acomulatorio_id, :complaint_type_id, :argumentos_auto_inhibitorio, :comentarios_jefe_oficina, :estado_proceso,
  :fecha_hechos, :hechos, :fecha_vencimiento_terminos, :texto_indagacion_preliminar, :argumentos_revision_competencia, 
  :comentarios_indagacion_preliminar, :viable, :iniciar_investigacion_disciplinaria, :cierre_investigacion_disciplinaria,
  :comentarios_jefe_oficina_investigacion_disciplinaria, :es_culpable, :auto_terminacion_archivo, :creador_id,
  :comentarios_profesional, :fecha_cita_audiencia, :comentarios_decision_culpabilidad, :comentarios_interponen_recursos, :nombre_implicado, :identificacion_implicado, 
  :cargo_implicado, :fecha_decision_tomada, :nombre_abogado_asignado, :identificacion_abogado_asignado, :n_tarjeta_profesional_abogado_asignado, 
  :fecha_dictado_sentencia, :fecha_ejecucion, :fecha_limite_ejecucion, :apuntes_audiencia, :audiencia_prorrogada, :decision_final_ejecutoriada, :se_interponen_recursos, :remitir_procedimiento_ordinario,
  :tipo_de_proceso, :nombre_del_implicado, :nombre_del_implicado, :lugar_de_los_hechos, :conducta_a_investigar,
  :cargo_del_implicado, :origen_de_queja, :se_presento_a_notificacion_personal, :solicitud_nulidad, :apelacion,:step,
 
 :ordinario_evaluacion_fecha_actuacion, 
 :ordinario_evaluacion_fecha_comunicacion,
 :ordinario_evaluacion_fecha_notificacion, 
 :ordinario_evaluacion_autoinhibitorio,
 :ordinario_evaluacion_remision_por_competencia, 
 :ordinario_evaluacion_citacion_audiencia, 
 :ordinario_evaluacion_indagacion_preliminar,
 :ordinario_evaluacion_apertura_investigacion, 
 :ordinario_evaluacion_nulidad, 
 :ordinario_evaluacion_nulidad_fecha_actuacion, 
 :ordinario_evaluacion_nulidad_fecha_comunicacion, 
 :ordinario_evaluacion_nulidad_fecha_notificacion,
 :ordinario_evaluacion_nulidad_recurso, 
 :ordinario_evaluacion_nulidad_recurso_actuacion,
 :ordinario_evaluacion_nulidad_recurso_comunicacion,
 :ordinario_evaluacion_nulidad_recurso_notificacion,
 :ordinario_evaluacion_nulidad_decretada, 
 
 :ordinario_indagacion_fecha_actuacion,
 :ordinario_indagacion_fecha_comunicacion, 
 :ordinario_indagacion_fecha_notificacion, 
 :ordinario_indagacion_citacion_audiencia, 
 :ordinario_indagacion_apertura_investigacion, 
 :ordinario_indagacion_terminacion_archivo_del_expediente, 
 :ordinario_indagacion_terminacion_recurso_de_apelacion, 
 :ordinario_indagacion_terminacion_fecha_actuacion, 
 :ordinario_indagacion_terminacion_fecha_comunicacion, 
 :ordinario_indagacion_terminacion_fecha_notificacion, 
 :ordinario_indagacion_terminacion_confirma, 
 :ordinario_indagacion_terminacion_revoca, 
 :ordinario_indagacion_nulidad, 
 :ordinario_indagacion_nulidad_fecha_actuacion, 
 :ordinario_indagacion_nulidad_fecha_comunicacion,
 :ordinario_indagacion_nulidad_fecha_notificacion, 
 :ordinario_indagacion_terminacion_archivo_confirma_actuacion, 
 :ordinario_indagacion_terminacion_archivo_confirma_comunicacion, 
 :ordinario_indagacion_terminacion_archivo_confirma_notificacion, 
 :ordinario_indagacion_nulidad_recurso,
 :ordinario_indagacion_nulidad_recurso_actuacion,
 :ordinario_indagacion_nulidad_recurso_comunicacion,
 :ordinario_indagacion_nulidad_recurso_notificacion,
 :ordinario_indagacion_nulidad_decretada, 
 :ordinario_indagacion_revocatoria_directa, 
 :ordinario_indagacion_revocatoria_directa_fecha_actuacion,
 :ordinario_indagacion_revocatoria_directa_fecha_comunicacion,
 :ordinario_indagacion_revocatoria_directa_fecha_notificacion, 
 :ordinario_indagacion_terminacion_fecha_actuacion_ap, 
 :ordinario_indagacion_terminacion_fecha_comunicacion_ap,
 :ordinario_indagacion_terminacion_fecha_notificacion_ap, 
 
 :ordinario_apertura_fecha_actuacion, 
 :ordinario_apertura_fecha_comunicacion, 
 :ordinario_apertura_fecha_notificacion, 
 :ordinario_apertura_prorroga_de_la_ivestigacion, 
 :ordinario_apertura_auto_cierre_de_investigacion, 
 :ordinario_apertura_prorroga, 
 :ordinario_apertura_terminacion_archivo_del_expediente, 
 :ordinario_apertura_terminacion_recurso_de_apelacion, 
 :ordinario_apertura_terminacion_fecha_actuacion, 
 :ordinario_apertura_terminacion_fecha_comunicacion, 
 :ordinario_apertura_terminacion_fecha_notificacion, 
 :ordinario_apertura_terminacion_confirma, 
 :ordinario_apertura_terminacion_revoca, 
 :ordinario_apertura_nulidad, 
 :ordinario_apertura_nulidad_fecha_actuacion, 
 :ordinario_apertura_nulidad_fecha_comunicacion, 
 :ordinario_apertura_nulidad_fecha_notificacion, 
 :ordinario_apertura_terminacion_archivo_confirma_actuacion, 
 :ordinario_apertura_terminacion_archivo_confirma_comunicacion, 
 :ordinario_apertura_terminacion_archivo_confirma_notificacion, 
 :ordinario_apertura_nulidad_recurso,
 :ordinario_apertura_nulidad_recurso_actuacion,
 :ordinario_apertura_nulidad_recurso_comunicacion,
 :ordinario_apertura_nulidad_recurso_notificacion,
 :ordinario_apertura_nulidad_decretada,
 :ordinario_apertura_revocatoria_directa,
 :ordinario_apertura_revocatoria_directa_fecha_actuacion, 
 :ordinario_apertura_revocatoria_directa_fecha_comunicacion,
 :ordinario_apertura_revocatoria_directa_fecha_notificacion, 
 :ordinario_apertura_terminacion_fecha_actuacion_ap, 
 :ordinario_apertura_terminacion_fecha_comunicacion_ap, 
 :ordinario_apertura_terminacion_fecha_notificacion_ap, 
 
 :ordinario_prorroga_fecha_actuacion, 
 :ordinario_prorroga_fecha_comunicacion, 
 :ordinario_prorroga_fecha_notificacion, 
 :ordinario_prorroga_auto_cierre_de_la_investigacion, 
 :ordinario_prorroga_terminacion_archivo_del_expediente, 
 :ordinario_prorroga_terminacion_recurso_de_apelacion, 
 :ordinario_prorroga_terminacion_fecha_actuacion, 
 :ordinario_prorroga_terminacion_fecha_comunicacion, 
 :ordinario_prorroga_terminacion_fecha_notificacion, 
 :ordinario_prorroga_terminacion_confirma, 
 :ordinario_prorroga_terminacion_revoca, 
 :ordinario_prorroga_nulidad, 
 :ordinario_prorroga_nulidad_fecha_actuacion, 
 :ordinario_prorroga_nulidad_fecha_comunicacion, 
 :ordinario_prorroga_nulidad_fecha_notificacion, 
 :ordinario_prorroga_terminacion_archivo_confirma_actuacion, 
 :ordinario_prorroga_terminacion_archivo_confirma_comunicacion,
 :ordinario_prorroga_terminacion_archivo_confirma_notificacion, 
 :ordinario_prorroga_nulidad_recurso,
 :ordinario_prorroga_nulidad_recurso_actuacion,
 :ordinario_prorroga_nulidad_recurso_comunicacion,
 :ordinario_prorroga_nulidad_recurso_notificacion,
 :ordinario_prorroga_nulidad_decretada, 
 :ordinario_prorroga_revocatoria_directa,
 :ordinario_prorroga_revocatoria_directa_fecha_actuacion, 
 :ordinario_prorroga_revocatoria_directa_fecha_comunicacion, 
 :ordinario_prorroga_revocatoria_directa_fecha_notificacion, 
 :ordinario_prorroga_terminacion_fecha_actuacion_ap, 
 :ordinario_prorroga_terminacion_fecha_comunicacion_ap, 
 :ordinario_prorroga_terminacion_fecha_notificacion_ap, 
 
 :ordinario_cierre_fecha_actuacion, 
 :ordinario_cierre_fecha_comunicacion, 
 :ordinario_cierre_fecha_notificacion, 
 :ordinario_cierre_pliego_de_cargos, 
 :ordinario_cierre_terminacion_archivo_del_expediente, 
 :ordinario_cierre_terminacion_recurso_de_apelacion, 
 :ordinario_cierre_terminacion_fecha_actuacion, 
 :ordinario_cierre_terminacion_fecha_comunicacion, 
 :ordinario_cierre_terminacion_fecha_notificacion,
 :ordinario_cierre_terminacion_terminacion_confirma, 
 :ordinario_cierre_terminacion_terminacion_revoca,
 :ordinario_cierre_nulidad, 
 :ordinario_cierre_nulidad_fecha_actuacion,
 :ordinario_cierre_nulidad_fecha_comunicacion, 
 :ordinario_cierre_nulidad_fecha_notificacion, 
 :ordinario_cierre_terminacion_archivo_confirma_actuacion, 
 :ordinario_cierre_terminacion_archivo_confirma_comunicacion, 
 :ordinario_cierre_terminacion_archivo_confirma_notificacion,
 :ordinario_cierre_nulidad_recurso,
 :ordinario_cierre_nulidad_recurso_actuacion,
 :ordinario_cierre_nulidad_recurso_comunicacion,
 :ordinario_cierre_nulidad_recurso_notificacion, 
 :ordinario_cierre_nulidad_decretada, 
 :ordinario_cierre_revocatoria_directa,
 :ordinario_cierre_revocatoria_directa_fecha_actuacion,
 :ordinario_cierre_revocatoria_directa_fecha_comunicacion, 
 :ordinario_cierre_revocatoria_directa_fecha_notificacion, 
 :ordinario_cierre_terminacion_fecha_actuacion_ap, 
 :ordinario_cierre_terminacion_fecha_comunicacion_ap, 
 :ordinario_cierre_terminacion_fecha_notificacion_ap, 
 
 :ordinario_pliego_de_cargos_fecha_actuacion, 
 :ordinario_pliego_de_cargos_fecha_comunicacion, 
 :ordinario_pliego_de_cargos_fecha_notificacion,
 :ordinario_pliego_de_cargos_segunda_fecha_actuacion, 
 :ordinario_pliego_de_cargos_segunda_fecha_comunicacion,
 :ordinario_pliego_de_cargos_segunda_fecha_notificacion, 
 :ordinario_pliego_de_cargos_nulidad, 
 :ordinario_pliego_de_cargos_nulidad_fecha_actuacion,
 :ordinario_pliego_de_cargos_nulidad_fecha_comunicacion,
 :ordinario_pliego_de_cargos_nulidad_fecha_notificacion,
 :ordinario_pliego_nulidad_recurso,
 :ordinario_pliego_nulidad_recurso_actuacion,
 :ordinario_pliego_nulidad_recurso_comunicacion,
 :ordinario_pliego_nulidad_recurso_notificacion,
 :ordinario_pliego_nulidad_decretada, 
 
 :ordinario_descargos_fecha_actuacion, 
 :ordinario_descargos_fecha_comunicacion, 
 :ordinario_descargos_fecha_notificacion,
 :ordinario_descargos_variacion_pliego_de_cargos,
 :ordinario_descargos_alegatos_de_conclusion,
 :ordinario_descargos_recurso_de_apelacion, 
 :ordinario_descargos_recurso_de_apelacion_fecha_actuacion, 
 :ordinario_descargos_recurso_de_apelacion_fecha_comunicacion, 
 :ordinario_descargos_recurso_de_apelacion_fecha_notificacion, 
 :ordinario_descargos_confirma, 
 :ordinario_descargos_revoca,
 :ordinario_descargos_terminacion_archivo_del_expediente, 
 :ordinario_descargos_terminacion_recurso_de_apelacion, 
 :ordinario_descargos_terminacion_fecha_actuacion, 
 :ordinario_descargos_terminacion_fecha_comunicacion, 
 :ordinario_descargos_terminacion_fecha_notificacion, 
 :ordinario_descargos_terminacion_confirma, 
 :ordinario_descargos_terminacion_terminacion_revoca, 
 :ordinario_descargos_nulidad, 
 :ordinario_descargos_nulidad_fecha_actuacion, 
 :ordinario_descargos_nulidad_fecha_comunicacion,
 :ordinario_descargos_nulidad_fecha_notificacion,
 :ordinario_descargos_terminacion_archivo_confirma_actuacion, 
 :ordinario_descargos_terminacion_archivo_confirma_comunicacion, 
 :ordinario_descargos_terminacion_archivo_confirma_notificacion, 
 :ordinario_descargos_nulidad_recurso,
 :ordinario_descargos_nulidad_recurso_actuacion,
 :ordinario_descargos_nulidad_recurso_comunicacion,
 :ordinario_descargos_nulidad_recurso_notificacion, 
 :ordinario_descargos_nulidad_decretada, 
 :ordinario_descargos_revocatoria_directa,
 :ordinario_descargos_revocatoria_directa_fecha_actuacion, 
 :ordinario_descargos_revocatoria_directa_fecha_comunicacion, 
 :ordinario_descargos_revocatoria_directa_fecha_notificacion, 
 :ordinario_descargos_terminacion_fecha_actuacion_ap, 
 :ordinario_descargos_terminacion_fecha_comunicacion_ap, 
 :ordinario_descargos_terminacion_fecha_notificacion_ap, 
 
 :ordinario_alegatos_fecha_actuacion, 
 :ordinario_alegatos_fecha_comunicacion,
 :ordinario_alegatos_fecha_notificacion,
 :ordinario_alegatos_fallo_en_primera_instancia, 
 :ordinario_alegatos_terminacion_archivo_del_expediente, 
 :ordinario_alegatos_terminacion_recurso_de_apelacion, 
 :ordinario_alegatos_terminacion_fecha_actuacion, 
 :ordinario_alegatos_terminacion_fecha_comunicacion,
 :ordinario_alegatos_terminacion_fecha_notificacion,
 :ordinario_alegatos_terminacion_terminacion_confirma, 
 :ordinario_alegatos_terminacion_terminacion_revoca, 
 :ordinario_alegatos_nulidad, 
 :ordinario_alegatos_nulidad_fecha_actuacion, 
 :ordinario_alegatos_nulidad_fecha_comunicacion, 
 :ordinario_alegatos_nulidad_fecha_notificacion, 
 :ordinario_alegatos_terminacion_archivo_confirma_actuacion,
 :ordinario_alegatos_terminacion_archivo_confirma_comunicacion, 
 :ordinario_alegatos_terminacion_archivo_confirma_notificacion,
 :ordinario_alegatos_nulidad_recurso,
 :ordinario_alegatos_nulidad_recurso_actuacion,
 :ordinario_alegatos_nulidad_recurso_comunicacion,
 :ordinario_alegatos_nulidad_recurso_notificacion,
 :ordinario_alegatos_nulidad_decretada, 
 :ordinario_alegatos_revocatoria_directa, 
 :ordinario_alegatos_revocatoria_directa_fecha_actuacion, 
 :ordinario_alegatos_revocatoria_directa_fecha_comunicacion, 
 :ordinario_alegatos_revocatoria_directa_fecha_notificacion,
 :ordinario_alegatos_terminacion_fecha_actuacion_ap, 
 :ordinario_alegatos_terminacion_fecha_comunicacion_ap, 
 :ordinario_alegatos_terminacion_fecha_notificacion_ap,
 
 :ordinario_fallo_en_primera_fecha_actuacion, 
 :ordinario_fallo_en_primera_fecha_comunicacion, 
 :ordinario_fallo_en_primera_fecha_notificacion, 
 :ordinario_fallo_en_primera_fallo_sancionatorio, 
 :ordinario_fallo_en_primera_fallo_absolutorio,
 :ordinario_fallo_en_primera_apelar, 
 :ordinario_fallo_en_primera_nulidad, 
 :ordinario_fallo_en_primera_nulidad_fecha_actuacion, 
 :ordinario_fallo_en_primera_nulidad_fecha_comunicacion, 
 :ordinario_fallo_en_primera_nulidad_fecha_notificacion, 
 :ordinario_fallo_en_primera_nulidad_recurso,
 :ordinario_fallo_en_primera_nulidad_recurso_actuacion, 
 :ordinario_fallo_en_primera_nulidad_recurso_comunicacion, 
 :ordinario_fallo_en_primera_nulidad_recurso_notificacion,
 :ordinario_fallo_en_primera_nulidad_decretada,
 :ordinario_fallo_en_primera_recurso, 
 :ordinario_fallo_en_primera_recurso_fecha_actuacion, 
 :ordinario_fallo_en_primera_recurso_fecha_comunicacion, 
 :ordinario_fallo_en_primera_recurso_fecha_notificacion, 
 :ordinario_fallo_en_primera_revocatoria, 
 :ordinario_fallo_en_primera_revocatoria_fecha_actuacion, 
 :ordinario_fallo_en_primera_revocatoria_fecha_comunicacion, 
 :ordinario_fallo_en_primera_revocatoria_fecha_notificacion, 
 
 :ordinario_fallo_en_segunda_fecha_actuacion, 
 :ordinario_fallo_en_segunda_fecha_comunicacion,
 :ordinario_fallo_en_segunda_fecha_notificacion, 
 :ordinario_fallo_en_segunda_nulidad, 
 :ordinario_fallo_en_segunda_nulidad_fecha_actuacion, 
 :ordinario_fallo_en_segunda_nulidad_fecha_comunicacion, 
 :ordinario_fallo_en_segunda_nulidad_fecha_notificacion,
 :ordinario_fallo_en_segunda_nulidad_recurso, 
 :ordinario_fallo_en_segunda_nulidad_recurso_actuacion, 
 :ordinario_fallo_en_segunda_nulidad_recurso_comunicacion,
 :ordinario_fallo_en_segunda_nulidad_recurso_notificacion,
 :ordinario_fallo_en_segunda_nulidad_decretada,

   
 :ordinario_nulidad_fecha_actuacion,
 :ordinario_nulidad_fecha_comunicacion,
 :ordinario_nulidad_fecha_notificacion,

 :ordinario_nulidad_accion,
   :verbal_nulidad_accion,
   :nulidad_en,
   :parent_id,
   :anulada,
   
   :etapa_del_expediente,
   
   :verbal_citacion_fecha_actuacion,
   :verbal_citacion_fecha_comunicacion, 
   :verbal_citacion_fecha_notificacion, 
   :verbal_citacion_nulidad,
   :verbal_citacion_nulidad_fecha_actuacion, 
   :verbal_citacion_nulidad_fecha_comunicacion, 
   :verbal_citacion_nulidad_fecha_notificacion, 
   :verbal_primera_fecha_actuacion, 
   :verbal_primera_fecha_comunicacion, 
   :verbal_primera_fecha_notificacion, 
   :verbal_primera_nulidad, 
   :verbal_primera_nulidad_fecha_actuacion, 
   :verbal_primera_nulidad_fecha_comunicacion, 
   :verbal_primera_nulidad_fecha_notificacion, 
   :verbal_primera_alegatos,
   :verbal_primera_alegatos_fecha_actuacion, 
   :verbal_primera_alegatos_fecha_comunicacion, 
   :verbal_primera_alegatos_fecha_notificacion, 
   :verbal_segunda_fecha_actuacion, 
   :verbal_segunda_fecha_comunicacion,
   :verbal_segunda_fecha_notificacion, 
   :verbal_segunda_apelacion, 
   :verbal_segunda_apelacion_actuacion, 
   :verbal_segunda_apelacion_comunicacion, 
   :verbal_segunda_apelacion_notificacion, 
   :verbal_segunda_apelacion_decretada, 
   :verbal_segunda_nulidad, 
   :verbal_segunda_nulidad_fecha_actuacion, 
   :verbal_segunda_nulidad_fecha_comunicacion, 
   :verbal_segunda_nulidad_fecha_notificacion,
   :verbal_citacion_nulidad_recurso, 
   :verbal_citacion_nulidad_recurso_actuacion, 
   :verbal_citacion_nulidad_recurso_comunicacion, 
   :verbal_citacion_nulidad_recurso_notificacion, 
   :verbal_citacion_nulidad_decretada,
   :verbal_primera_nulidad_recurso, 
   :verbal_primera_nulidad_recurso_actuacion, 
   :verbal_primera_nulidad_recurso_comunicacion,
   :verbal_primera_nulidad_recurso_notificacion, 
   :verbal_primera_nulidad_decretada,
   :verbal_primera_alegatos, 
   :verbal_primera_alegatos_fecha_actuacion, 
   :verbal_primera_alegatos_fecha_comunicacion, 
   :verbal_primera_alegatos_fecha_notificacion,
   :verbal_segunda_nulidad_recurso, 
   :verbal_segunda_nulidad_recurso_actuacion, 
   :verbal_segunda_nulidad_recurso_comunicacion, 
   :verbal_segunda_nulidad_recurso_notificacion,
   :verbal_segunda_nulidad_decretada,
   :consecutivo,
   :etapa_id,
   :nulidad_etapa_id,
   :nulidad_accion_id,
   :ultima_actuacion,
   :fecha_limite,
   :verbal_decreto_de_pruebas, 
   :verbal_decreto_de_pruebas_fecha_actuacion,
   :verbal_decreto_de_pruebas_fecha_comunicacion,
   :verbal_decreto_de_pruebas_fecha_notificacion,
  
   :campo_de_la_ultima_actuacion,
   :fecha_queja,
   :fecha_recibido_en_esta_dependencia,
   :cedula_implicado, 
   :entidad,
   :municipio,
   :cedula_disciplinado,
   :entidad, 
   :municipio,
   :disciplinado,
   :estado_de_tramite,
   :verbal_evaluacion_fecha_actuacion, 
   :verbal_evaluacion_fecha_comunicacion, 
   :verbal_evaluacion_fecha_notificacion, 
   :verbal_evaluacion_autoinhibitorio,
   :verbal_evaluacion_remision_por_competencia, 
   :verbal_evaluacion_citacion_audiencia,
   :verbal_evaluacion_indagacion_preliminar, 
   :verbal_evaluacion_apertura_investigacion, 
   :verbal_evaluacion_nulidad, 
   :verbal_evaluacion_nulidad_fecha_actuacion, 
   :verbal_evaluacion_nulidad_fecha_comunicacion, 
   :verbal_evaluacion_nulidad_fecha_notificacion, 
   :verbal_evaluacion_nulidad_recurso, 
   :verbal_evaluacion_nulidad_recurso_actuacion, 
   :verbal_evaluacion_nulidad_recurso_comunicacion, 
   :verbal_evaluacion_nulidad_recurso_notificacion, 
   :verbal_evaluacion_nulidad_decretada, 
   :verbal_indagacion_fecha_actuacion, 
   :verbal_indagacion_fecha_comunicacion, 
   :verbal_indagacion_fecha_notificacion, 
   :verbal_indagacion_citacion_audiencia, 
   :verbal_indagacion_apertura_investigacion, 
   :verbal_indagacion_terminacion_archivo_del_expediente, 
   :verbal_indagacion_terminacion_recurso_de_apelacion, 
   :verbal_indagacion_terminacion_fecha_actuacion, 
   :verbal_indagacion_terminacion_fecha_comunicacion, 
   :verbal_indagacion_terminacion_fecha_notificacion, 
   :verbal_indagacion_terminacion_confirma, 
   :verbal_indagacion_terminacion_revoca, 
   :verbal_indagacion_nulidad,
   :verbal_indagacion_nulidad_fecha_actuacion, 
   :verbal_indagacion_nulidad_fecha_comunicacion, 
   :verbal_indagacion_nulidad_fecha_notificacion,
   :verbal_indagacion_terminacion_archivo_confirma_actuacion, 
   :verbal_indagacion_terminacion_archivo_confirma_comunicacion, 
   :verbal_indagacion_terminacion_archivo_confirma_notificacion, 
   :verbal_indagacion_nulidad_recurso, 
   :verbal_indagacion_nulidad_recurso_actuacion, 
   :verbal_indagacion_nulidad_recurso_comunicacion, 
   :verbal_indagacion_nulidad_recurso_notificacion, 
   :verbal_indagacion_nulidad_decretada, 
   :verbal_indagacion_revocatoria_directa, 
   :verbal_indagacion_revocatoria_directa_fecha_actuacion, 
   :verbal_indagacion_revocatoria_directa_fecha_comunicacion, 
   :verbal_indagacion_revocatoria_directa_fecha_notificacion, 
    :verbal_apertura_fecha_actuacion, 
    :verbal_apertura_fecha_comunicacion, 
    :verbal_apertura_fecha_notificacion, 
    :verbal_apertura_prorroga_de_la_ivestigacion, 
    :verbal_apertura_auto_cierre_de_investigacion, 
    :verbal_apertura_prorroga, 
    :verbal_apertura_terminacion_archivo_del_expediente, 
    :verbal_apertura_terminacion_recurso_de_apelacion, 
    :verbal_apertura_terminacion_fecha_actuacion, 
    :verbal_apertura_terminacion_fecha_comunicacion, 
    :verbal_apertura_terminacion_fecha_notificacion, 
    :verbal_apertura_terminacion_confirma, 
    :verbal_apertura_terminacion_revoca, 
    :verbal_apertura_nulidad, 
    :verbal_apertura_nulidad_fecha_actuacion, 
    :verbal_apertura_nulidad_fecha_comunicacion, 
    :verbal_apertura_nulidad_fecha_notificacion, 
    :verbal_apertura_terminacion_archivo_confirma_actuacion, 
    :verbal_apertura_terminacion_archivo_confirma_comunicacion,
    :verbal_apertura_terminacion_archivo_confirma_notificacion, 
    :verbal_apertura_nulidad_recurso, 
    :verbal_apertura_nulidad_recurso_actuacion,
    :verbal_apertura_nulidad_recurso_comunicacion, 
    :verbal_apertura_nulidad_recurso_notificacion,
    :verbal_apertura_nulidad_decretada, 
    :verbal_alegatos_revocatoria_directa, 
    :verbal_alegatos_revocatoria_directa_fecha_actuacion, 
    :verbal_alegatos_revocatoria_directa_fecha_comunicacion, 
    :verbal_alegatos_revocatoria_directa_fecha_notificacion, 
    :verbal_prorroga_fecha_actuacion, 
    :verbal_prorroga_fecha_comunicacion, 
    :verbal_prorroga_fecha_notificacion, 
    :verbal_prorroga_auto_cierre_de_la_investigacion, 
    :verbal_prorroga_terminacion_archivo_del_expediente, 
    :verbal_prorroga_terminacion_recurso_de_apelacion, 
    :verbal_prorroga_terminacion_fecha_actuacion, 
    :verbal_prorroga_terminacion_fecha_comunicacion, 
    :verbal_prorroga_terminacion_fecha_notificacion, 
    :verbal_prorroga_terminacion_confirma, 
    :verbal_prorroga_terminacion_revoca, 
    :verbal_prorroga_nulidad, 
    :verbal_prorroga_nulidad_fecha_actuacion,
    :verbal_prorroga_nulidad_fecha_comunicacion, 
    :verbal_prorroga_nulidad_fecha_notificacion,
    :verbal_prorroga_terminacion_archivo_confirma_actuacion, 
    :verbal_prorroga_terminacion_archivo_confirma_comunicacion, 
    :verbal_prorroga_terminacion_archivo_confirma_notificacion, 
    :verbal_prorroga_nulidad_recurso, 
    :verbal_prorroga_nulidad_recurso_actuacion, 
    :verbal_prorroga_nulidad_recurso_comunicacion, 
    :verbal_prorroga_nulidad_recurso_notificacion,
    :verbal_prorroga_nulidad_decretada, 
    :verbal_prorroga_revocatoria_directa, 
    :verbal_prorroga_revocatoria_directa_fecha_actuacion, 
    :verbal_prorroga_revocatoria_directa_fecha_comunicacion,
    :verbal_prorroga_revocatoria_directa_fecha_notificacion,
    :verbal_apertura_revocatoria_directa,
    :verbal_apertura_revocatoria_directa_fecha_actuacion, 
    :verbal_apertura_revocatoria_directa_fecha_comunicacion, 
    :verbal_apertura_revocatoria_directa_fecha_notificacion,
    :ano,
    :nombre_del_quejoso,
    :cedula_del_quejoso,
    
   
    :nombre_de_abogado_asignado,
    :fecha_de_los_hechos_por_establecer,
    :fecha_de_recibido,
    :archivado
    
    
  attr_accessor :ofiles_hash
 
  
  has_many :users_complaints
  has_many :users, through: :users_complaints
  has_many :ofiles
  belongs_to :complaint_type
  belongs_to :etapa
  
  #Si es citado a indagacion cambia tipo de proceso
  before_update :logica_expedientes
  #Aca se maneja la logica de las nulidades
  before_update :nulidades_logica
  #Detecta cambios en los campos usando metaprogramming
  before_save :hook_de_cambio_en_campos
  #fecha_limite logica, automatizacion de las etapas con las actuaciones
  before_save :hook_de_actuaciones
  
  
  def nulidad_accion
    if self.nulidad_accion_id
      etapa = Etapa.find(self.nulidad_accion_id)
      etapa.name
    end
  end
  
  def nulidad_etapa
    if self.nulidad_accion_id
      etapa = Etapa.find(self.nulidad_etapa_id)
      return etapa.name
    end
  end
  
  
  def dias_prescripcion
    if self.fecha_hechos && !self.name
      
      option_one = DateTime.new(2011,7,12)
      option_two = DateTime.new(2017,1,1)
      now = DateTime.now
      
      if self.fecha_hechos < option_one
        fecha_limite_aux = self.fecha_hechos + 5.year
        dias_restantes = fecha_limite_aux - now
      elsif self.fecha_hechos > option_two
        fecha_limite_aux = self.fecha_hechos + 5.year
        dias_restantes = fecha_limite_aux - now
      elsif self.fecha_hechos > option_one
        if self.ordinario_apertura_fecha_actuacion
          fecha_limite_aux = self.ordinario_apertura_fecha_actuacion + 5.year
          dias_restantes = fecha_limite_aux - now
        end
      end
      return dias_restantes.to_i/86400
    end  
    
  end
  
  def dias_caducidad
    
    if self.fecha_hechos && !self.ordinario_apertura_fecha_actuacion 
      now = DateTime.now
      fecha_limite_aux = self.fecha_hechos + 5.year
      dias_restantes = fecha_limite_aux - now
      
      return dias_restantes.to_i/86400
      
    end  
    
  end
  
  
  def dias_tramite
    if self.fecha_queja && !self.name
		
      endDate = Date.parse(Time.now.to_s)
      beginDate = self.fecha_queja
      value = endDate - beginDate
      value = value.to_i
   
    end
  end
  
  def obtener_id_de_etapa(etapa,tipo)
    etapa = Etapa.where("name = ? and tipo = ?",etapa,tipo).first
    if etapa
      return etapa.id
    else
      return nil
    end
  end
  
  def hook_de_actuaciones
    
    if self.ordinario_evaluacion_fecha_actuacion_changed?
      if self.ordinario_evaluacion_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Evaluación de la noticia disciplinaria","ordinario")
       # self.fecha_limite = self.ordinario_indagacion_fecha_actuacion + 6.months 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_indagacion_fecha_actuacion_changed?
      if self.ordinario_indagacion_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Indagación preliminar","ordinario")
        self.fecha_limite = self.ordinario_indagacion_fecha_actuacion + 6.months 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_apertura_fecha_actuacion_changed?
      if self.ordinario_apertura_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Apertura de la investigación disciplinaria","ordinario")
        self.fecha_limite = self.ordinario_apertura_fecha_actuacion + 12.months 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_prorroga_fecha_actuacion_changed?
      if self.ordinario_prorroga_fecha_actuacion && !self.anulada && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Prorroga de la investigación disciplinaria","ordinario")
        self.fecha_limite = self.ordinario_prorroga_fecha_actuacion + 6.months
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_cierre_fecha_actuacion_changed?
      if self.ordinario_cierre_fecha_actuacion && !self.anulada
         self.etapa_id = obtener_id_de_etapa("Cierre de la investigación","ordinario")
        self.fecha_limite = self.ordinario_cierre_fecha_actuacion + 20.days 
      else
        self.fecha_limite = nil
      end
    
    elsif self.ordinario_pliego_de_cargos_fecha_actuacion_changed?
      if self.ordinario_pliego_de_cargos_fecha_actuacion && !self.anulada
         self.etapa_id = obtener_id_de_etapa("Pliego de cargos","ordinario")
        #self.fecha_limite = self.ordinario_cierre_fecha_actuacion + 20.days 
      else
        self.fecha_limite = nil
      end
      
    elsif self.ordinario_descargos_fecha_actuacion_changed?
      if self.ordinario_descargos_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Descargos","ordinario")
        self.fecha_limite = self.ordinario_descargos_fecha_actuacion + 90.days 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_alegatos_fecha_actuacion_changed?
      if self.ordinario_alegatos_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Alegatos de conclusión","ordinario")
        self.fecha_limite = self.ordinario_alegatos_fecha_actuacion + 14.days 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_fallo_en_primera_fecha_actuacion_changed?
      if self.ordinario_fallo_en_primera_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Fallo de primera instancia","ordinario")
        self.fecha_limite = self.ordinario_fallo_en_primera_fecha_actuacion + 30.days 
      else
        self.fecha_limite = nil
      end
    elsif self.ordinario_fallo_en_segunda_fecha_actuacion_changed?
      if self.ordinario_fallo_en_segunda_fecha_actuacion && !self.anulada
        self.etapa_id = obtener_id_de_etapa("Fallo de segunda instancia","ordinario")
        self.fecha_limite = self.ordinario_fallo_en_segunda_fecha_actuacion + 45.days 
      else
        self.fecha_limite = nil
      end
    end
  end

  
  def hook_de_cambio_en_campos
    
    changed_fields = self.changed
    puts "Campos cambiados"
    puts changed_fields
    
    for field in changed_fields
    
      if field.include? "actuacion"
        self.ultima_actuacion = eval("self.#{field}") 
        self.campo_de_la_ultima_actuacion = field
      end
      
      if field.include?("ordinario_indagacion_terminacion_archivo_del_expediente") || field.include?("ordinario_apertura_terminacion_archivo_del_expediente") || field.include?("ordinario_prorroga_terminacion_archivo_del_expediente")|| field.include?("ordinario_cierre_terminacion_archivo_del_expediente") || field.include?("ordinario_descargos_terminacion_archivo_del_expediente") || field.include?("ordinario_descargos_terminacion_archivo_del_expediente") 
        if eval("self.#{field}") == true
          self.archivado = true
        end
      end
      
    end
  
  end

  
  def nulidades_logica
    
    #1 Se clona el modelo como esta en el momento 0
    #2 Se borran todos los campos en el original hasta la etapa a la que devolvio
    #3 Se establece la etapa donde devolvio como la etapa actual del modelo editado
   
    if self.nulidad_etapa_id

      if self.ordinario_evaluacion_nulidad_decretada_changed?
        if self.ordinario_evaluacion_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 1
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 1
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Evaluación de la noticia disciplinaria","ordinario")
          self.limpiar_campos("Evaluación de la noticia disciplinaria","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_indagacion_nulidad_decretada_changed?
        if self.ordinario_indagacion_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 2
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 2
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Indagación preliminar","ordinario")
          self.limpiar_campos("Indagación preliminar","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_apertura_nulidad_decretada_changed?
        if self.ordinario_apertura_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 3
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 3
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Apertura de la investigación disciplinaria","ordinario")
          self.limpiar_campos("Apertura de la investigación disciplinaria","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_prorroga_nulidad_decretada_changed?
        if self.ordinario_prorroga_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 4
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 4
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Prorroga de la investigación disciplinaria","ordinario")
          self.limpiar_campos("Prorroga de la investigación disciplinaria","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_cierre_nulidad_decretada_changed?
        if self.ordinario_cierre_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 5
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 5
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Cierre de la investigación","ordinario")
          self.limpiar_campos("Cierre de la investigación","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_pliego_nulidad_decretada_changed?
        if self.ordinario_pliego_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 6
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 6
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Pliego de cargos","ordinario")
          self.limpiar_campos("Pliego de cargos","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_descargos_nulidad_decretada_changed?
        if self.ordinario_descargos_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 7
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 7
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Descargos","ordinario")
          self.limpiar_campos("Descargos","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_alegatos_nulidad_decretada_changed?
        if self.ordinario_alegatos_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 8
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 8
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Alegatos de conclusión","ordinario")
          self.limpiar_campos("Alegatos de conclusión","ordinario")
          self.etapa_id = self.nulidad_etapa_id
         
        end
      elsif self.ordinario_fallo_en_primera_nulidad_decretada_changed?
        if self.ordinario_fallo_en_primera_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 9
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 9
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Fallo de primera instancia","ordinario")
          self.limpiar_campos("Fallo de primera instancia","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      elsif self.ordinario_fallo_en_segunda_nulidad_decretada_changed?
        if self.ordinario_fallo_en_segunda_nulidad_decretada == "Decretada"
          
          complaint_anulado = self.dup
          complaint_anulado.nulidad_accion_id = 10
          complaint_anulado.parent_id = self.id
          complaint_anulado.anulada = true
          complaint_anulado.etapa_id = 10
          complaint_anulado.save
          
          self.etapa_id = obtener_id_de_etapa("Fallo de segunda instancia","ordinario")
          self.limpiar_campos("Fallo de segunda instancia","ordinario")
          self.etapa_id = self.nulidad_etapa_id
          
        end
      end
      
      #self.etapa_id = self.nulidad_etapa_id
      
    end 
    true
    
  end
  
  def limpiar_campos(etapa,tipo)
    #Este metodo se utiliza para limpiar los campos de el complaint que ha sido anulado
    
    etapa_campos = ""
    etapas = Etapa.where("tipo = ?",tipo).index_by(&:name)
    
   if etapa == "Evaluación de la noticia disciplinaria"
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos
   elsif etapa == "Indagación preliminar"
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos
    elsif etapa == "Apertura de la investigación disciplinaria"
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
    elsif etapa == "Prorroga de la investigación disciplinaria"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos
    elsif etapa == "Cierre de la investigación"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos
    elsif etapa == "Pliego de cargos"     
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos+ ","
      etapa_campos = etapa_campos + etapas["Pliego de cargos"].campos+ ","
    elsif etapa == "Descargos"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos+ ","
      etapa_campos = etapa_campos + etapas["Pliego de cargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Descargos"].campos
    elsif etapa == "Alegatos de conclusión"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos + ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos+ ","
      etapa_campos = etapa_campos + etapas["Pliego de cargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Descargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Alegatos de conclusión"].campos
    elsif etapa == "Fallo de primera instancia"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos+ ","
      etapa_campos = etapa_campos + etapas["Pliego de cargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Descargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Fallo de primera instancia"].campos
    elsif etapa == "Fallo de segunda instancia"
      etapa_campos = etapa_campos + etapas["Prorroga de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Evaluación de la noticia disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Indagación preliminar"].campos+ ","
      etapa_campos = etapa_campos + etapas["Apertura de la investigación disciplinaria"].campos+ ","
      etapa_campos = etapa_campos + etapas["Cierre de la investigación"].campos+ ","
      etapa_campos = etapa_campos + etapas["Pliego de cargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Descargos"].campos+ ","
      etapa_campos = etapa_campos + etapas["Fallo de primera instancia"].campos+ ","
      etapa_campos = etapa_campos + etapas["Fallo de segunda instancia"].campos
    end
    
    etapa_campos.gsub! ' ',''
    array_de_campos = etapa_campos.split(",")
    #recorremos el array borrando los fields
    
    for ocampo in array_de_campos
      puts "ocampo: self.#{ocampo} = nil" 
      eval("self.#{ocampo} = nil")
    end
    
  end
  
  
  def logica_expedientes
     
    if self.ordinario_evaluacion_citacion_audiencia_changed?
       if self.ordinario_evaluacion_citacion_audiencia
        self.tipo_de_proceso = "PROCESO VERBAL"
      end
    end
    
    if self.ordinario_indagacion_citacion_audiencia_changed?
      if self.ordinario_indagacion_citacion_audiencia
       self.tipo_de_proceso = "PROCESO VERBAL"
     end
   end
   
   true
  end
  

  
  def fecha_tramite
    if !self.etapa_id || self.etapa_id == 1  
      if self.fecha_queja.class == Date
        endDate = Date.parse(Time.now.to_s)
        beginDate = self.fecha_queja
        r = endDate - beginDate
        r.to_i
      end
    else
      "Tramitado"
    end
  end
  
  
  
  
  #Class methods
  
  def archivar_expedientes
    self.attributes.each_pair do |name, value|
      
      if(name == "ordinario_indagacion_terminacion_archivo_del_expediente") || (name =="ordinario_apertura_terminacion_archivo_del_expediente") || (name =="ordinario_prorroga_terminacion_archivo_del_expediente")|| (name =="ordinario_cierre_terminacion_archivo_del_expediente") || (name =="ordinario_descargos_terminacion_archivo_del_expediente") || (name =="ordinario_descargos_terminacion_archivo_del_expediente") 
        puts "entro"
        if value == true
          self.archivado = true
          puts "expediente #{self.consecutivo} Archivado"
          self.save
        end
      end
    end
  end
  
  
  def self.en_tramite
    
    sqltramite = "select c.fecha_hechos, c.archivado, c.tipo_de_proceso, c.id, u.email  from complaints c inner join users_complaints uc on c.id = uc.complaint_id 
    left join users u on u.id = uc.user_id where c.archivado is not true"
    
    return sqltramite
    
  end
  
  
  def self.no_asignadas
    
    sql = "select c.fecha_hechos, c.tipo_de_proceso, c.id, u.email  from complaints c inner join users_complaints uc on c.id != uc.complaint_id 
    left join users u on u.id = uc.user_id"
    
    sql
    
  end
  
  def self.procedimiento_ordinario
    
    sql = "select c.fecha_hechos, c.tipo_de_proceso, c.id, u.email  from complaints c inner join users_complaints uc on c.id != uc.complaint_id 
    left join users u on u.id = uc.user_id"
    
    sql
    
  end
  
  def self.procedimiento_verbal
    
    sql = "select c.fecha_hechos, c.tipo_de_proceso, c.id, u.email from complaints where c.tipo_de_proceso == 'Proceso Verbal' ORDER BY c.id DESC "
    
    sql
    
  end
  
  def self.procedimiento_ordinario
    
    sql = "select c.fecha_hechos, c.tipo_de_proceso, c.id, u.email from complaints c left join users_complaints uc on c.id = uc.complaint_id 
    left join users u on u.id = uc.user_id
    ORDER BY c.id DESC where c.tipo_de_proceso == 'Proceso Ordinario'"
    
    sql
    
  end

  
  
end
