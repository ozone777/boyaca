class Ofile < ActiveRecord::Base
  attr_accessible :name, :step
  
  attr_accessible :avatar, :complaint_id
  has_attached_file :avatar
  validates_attachment_content_type :avatar, content_type: /\w/
  validates :avatar_file_name, :presence => true
  
  belongs_to :complaint
  #validates_attachment :image, content_type: { content_type: /\Aimage\/.*\Z/ }
  
end
