class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_create :generate_token
  
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :company, :first_name, :last_name, :role_id, :message, :user_id, :current_password
  attr_accessor :current_password
  
  validates_presence_of :email
  
  validates :password, presence: true, length: {minimum: 5, maximum: 120}, on: :create
  validates :password, length: {minimum: 5, maximum: 120}, on: :update, allow_blank: true
  
  belongs_to :role
  
  has_many :users_complaints
  has_many :complaints, through: :users_complaints
  
  

  # has_many :suppliers_users, :class_name => "SharedBrief", :foreign_key => 'answer_id'
#   has_many :clients_users, :class_name => "SharedBrief", :foreign_key => 'asker_id'
#
#   has_many :clients, :through => :clients_users, :source => :answer
#   has_many :suppliers, :through => :suppliers_users, :source => :asker
#
#   has_many :briefs, :class_name => "Brief", :foreign_key => 'creator_id'
  
  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.exists?(token: random_token)
    end
  end
  
  def superadmin?
    if self.role_id == 5
      true
    else
      false
    end
  end
  
  def complete_name
    aux =""
    aux = self.first_name if self.first_name
    aux = aux + self.last_name if self.last_name
    aux
  end
  
  #5 Administrador
  #1 Auxiliar Administrativo (Crea quejas)
  #2 Jefe oficina
  #3 Profesional
  
  def admin?
    if self.role_id == 5 || self.role_id == 4
      true
    else
      false
    end
  end
  
  def aux_administrativo?
    role_id == 1 ? true : false
  end
  
  def jefe_oficina?
    if self.role_id == 2
      true
    else
      false
    end
  end
  
  def profesional?
    if self.role_id == 3
      true
    else
      false
    end
  end
  
  def get_role
    self.role.name
  end

  def briefs_count
    Brief.find_by_sql("SELECT b.id FROM briefs b INNER JOIN shared_briefs s ON s.brief_id = b.id WHERE (s.answer_id = #{self.id} OR s.asker_id = #{self.id})").count
  end
  
  def briefs_client_review_count
    Brief.find_by_sql("SELECT b.id FROM briefs b WHERE b.creator_id =  #{self.id} AND status_id < 4").count
  end
  
  def briefs_finished_count
    Brief.find_by_sql("SELECT b.id FROM briefs b WHERE b.creator_id =  #{self.id} AND status_id = 4").count
  end
  
  def clients_count
    Brief.find_by_sql("SELECT (s.answer_id) FROM shared_briefs s WHERE s.asker_id = #{self.id} AND s.answer_id <> #{self.id} GROUP BY s.answer_id").count
  end
  
  # has_many :briefs_client_approval, :conditions => ['status_id = 3'], :class_name => "Brief", :foreign_key => 'creator_id'
#   has_many :briefs_creative_review, :conditions => ['status_id = 2'],:class_name => "Brief", :foreign_key => 'creator_id'
#   has_many :briefs_client_review, :conditions => ['status_id = 1'], :class_name => "Brief", :foreign_key => 'creator_id'
#   has_many :briefs_finished,  :conditions => ['status_id = 4'], :class_name => "Brief", :foreign_key => 'creator_id'

end
