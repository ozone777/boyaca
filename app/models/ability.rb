class Ability
  include CanCan::Ability
  
  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      user ||= User.new # guest user (not logged in)
       if user.admin?
         can :manage, :all
      
      elsif user.aux_administrativo?
        can :new, Complaint
        can :create, Complaint
        can :update, Complaint
        can :search, Complaint
        can :no_asignadas, Complaint
        can :en_tramite, Complaint
        can :procedimiento_ordinario, Complaint
        can :procedimiento_verbal, Complaint
        can :estadisticas, Complaint
        can :read, :all
        
      elsif user.profesional? 
        
         can :search, Complaint
         can :edit, Complaint
         can :update, Complaint
         can :update, User
         
         can :no_asignadas, Complaint
         can :en_tramite, Complaint
         can :procedimiento_ordinario, Complaint
         can :procedimiento_verbal, Complaint
         can :estadisticas, Complaint
         can :read, :all
         
      elsif user.jefe_oficina? 
        can :search, Complaint
        can :edit, Complaint
        can :assign, Complaint
        can :save_assign, Complaint
        can :update, Complaint
        can :no_asignadas, Complaint
        can :en_tramite, Complaint
        can :procedimiento_ordinario, Complaint
        can :procedimiento_verbal, Complaint
        can :estadisticas, Complaint
        can :read, :all
       else
         can :read, :all
       end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
