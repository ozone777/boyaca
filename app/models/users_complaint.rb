class UsersComplaint < ActiveRecord::Base
  attr_accessible :complaint_id, :user_id
  
  validates_uniqueness_of :user_id, :scope => :complaint_id #can't be a member in the same group twice
  
  belongs_to :user
  belongs_to :complain
end
