class GobVar < ActiveRecord::Base
  attr_accessible :meta_key, :meta_value
  
  validates :meta_key, :presence => true
  validates :meta_value, :presence => true
  
  def self.meta(meta_key)
    object = GobVar.where("meta_key = ?",meta_key).first
    if object
      object.try(:meta_value)
    else
      false
    end
  end
  
end
