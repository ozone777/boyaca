class GobDoc < ActiveRecord::Base
  
  attr_accessible :dpto, :name, :avatar
  
  has_attached_file :avatar
  
  validates_attachment_presence :avatar
  
  validates_attachment_content_type :avatar, :content_type => /\w/
  
  
  def process_doc
    
    if Rails.env.development?
      urldef = "http://localhost:3000"+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
    else  
      urldef = GobVar.meta("domain")+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
      #urldef = "http://boyaca.ozonegroup.co"+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
    end
    
    puts "/////////////////////////////"
    puts "/////////////////////////////"
    puts "Estoy probando MR OZONE"
    puts urldef
    
    origenes_de_quejas = ["DE OFICIO","POR PARTICULAR","QUEJOSO","INFORME DE SERVIDOR PUBLICO","POR ANONIMO"]
    #h = {'dog' => 'canine', 'cat' => 'feline', 'donkey' => 'asinine', 12 => 'dodecine'} 
    
    ousers = User.all
    abogados = Hash.new
    
    for ouser in ousers
      abogados["#{ouser.first_name}"] = ouser.id
    end
    #abogados = {"STELLA LEGUIZAMON" => 7,"EDWIN FONSECA" => 8, "ANGELA SACRISTAN" => 10,"FREDY OSWALDO PEREZ" => 11,"LUIS CARLOS BERNAL MESA" => 13,"LAURA MARCELA ALFONSO" => 14,"JUAN CARLOS HERNANDEZ" => 15,"JHONNY ALEXANDER ORTIZ" => 16,"ADRIANA CONSTANZA FAJARDO" => 17,"NIDIA MILENA BARON" =>18}
    
    oo =  Roo::Excel.new(urldef) 
    oo.default_sheet = oo.sheets[1]
    rows = 1
    2.upto(oo.last_row) do |line|
      
      begin
      
         puts "ROW Number: #{rows}"
         puts "A: #{oo.cell(line,'A')}"
         puts "B: #{oo.cell(line,'B')}"
         puts "C: #{oo.cell(line,'C')}"
         puts "D: #{oo.cell(line,'D')}"
         puts "E: #{oo.cell(line,'E')}"
         puts "F: #{oo.cell(line,'F')}"
         puts "G: #{oo.cell(line,'G')}"
         puts "H: #{oo.cell(line,'H')}"
         puts "I: #{oo.cell(line,'I')}"
         puts "J: #{oo.cell(line,'J')}"
         puts "K: #{oo.cell(line,'K')}"
         rows = rows + 1
         
         complaint = Complaint.new
         complaint.consecutivo = oo.cell(line,'A')
         
         aux = "#{oo.cell(line,'C')}"
         if !aux.include?("POR") && !aux.include?("INDE")
           complaint.fecha_hechos = oo.cell(line,'C')
         elsif aux.include?("POR")
           complaint.fecha_hechos_string = "POR ESTABLECER"
         elsif aux.include?("INDE")
           complaint.fecha_hechos_string = "INDETERMINADA"
         end
         
         aux = "#{oo.cell(line,'D')}"
         if !aux.include?("POR") && !aux.include?("INDE")
           complaint.fecha_queja = oo.cell(line,'D')
         elsif aux.include?("POR")
           complaint.fecha_queja_string = "POR ESTABLECER"
         elsif aux.include?("INDE")
           complaint.fecha_queja_string = "INDETERMINADA"
         end
         
         
         if oo.cell(line,'E').class == Date
           complaint.fecha_recibido_en_esta_dependencia = oo.cell(line,'E')
         elsif oo.cell(line,'E').class == Float
           complaint.fecha_recibido_en_esta_dependencia = Date.strptime("#{oo.cell(line,'E')}", "%Y")
         end
         
         complaint.origen_de_queja = oo.cell(line,'F')
         complaint.disciplinado = oo.cell(line,'G')
         complaint.cedula_disciplinado = oo.cell(line,'H').to_i
         complaint.cargo_del_implicado = oo.cell(line,'I')
         complaint.entidad = oo.cell(line,'J')
         complaint.municipio = oo.cell(line,'K')
         complaint.municipio = oo.cell(line,'K')
         complaint.conducta_a_investigar = oo.cell(line,'L')
         complaint.tipo_de_proceso = "PROCESO ORDINARIO"
         
         complaint.ano = oo.cell(line,'B').to_i
         complaint.origen_de_queja = oo.cell(line,'F')
         
         if !origenes_de_quejas.include? oo.cell(line,'F')
           complaint.nombre_del_quejoso = oo.cell(line,'F')
           complaint.origen_de_queja = "QUEJOSO"
         end
         
         #Actuaciones Ordinario migracion
         if oo.cell(line,'M').class == Date
         #Indagacion preliminar
           complaint.ordinario_indagacion_fecha_actuacion = oo.cell(line,'M')
           complaint.etapa_id = 2
         end
         
         if oo.cell(line,'N').class == Date
         #APERTURA DE LA INVESTIGACIÓN DISCIPLINARIA
           complaint.ordinario_apertura_fecha_actuacion = oo.cell(line,'N')
           complaint.etapa_id = 3
         end
         
         if oo.cell(line,'O').class == Date
         #Prorroga de la investigacion disciplinaria
           complaint.ordinario_prorroga_fecha_actuacion = oo.cell(line,'O')
           complaint.etapa_id = 4
         end
         
         if oo.cell(line,'P').class == Date
           #Cierre de la investigacion
           complaint.ordinario_cierre_fecha_actuacion = oo.cell(line,'P')
           complaint.etapa_id = 5
         end
         
         if oo.cell(line,'Q').class == Date
           #Pliego de cargos
           complaint.ordinario_pliego_de_cargos_fecha_actuacion = oo.cell(line,'Q')
           complaint.etapa_id = 6
         end
         
         if oo.cell(line,'R').class == Date
           complaint.ordinario_descargos_fecha_actuacion = oo.cell(line,'R')
           complaint.etapa_id = 7
         end
           
         if oo.cell(line,'S').class == Date
           
         end
           
         if oo.cell(line,'T').class == Date
           #Alegatos de conclusion
           complaint.ordinario_alegatos_fecha_actuacion = oo.cell(line,'T')
           complaint.etapa_id = 8
         end
         
         if oo.cell(line,'U').class == Date
           #Primera instancia
           complaint.ordinario_fallo_en_primera_fecha_actuacion = oo.cell(line,'U')
           complaint.etapa_id = 9
         end
         
         #Logica de migracion terminacion y archivo
         
         if oo.cell(line,'X').class == Date
         
         if complaint.etapa_id = 2
           complaint.ordinario_indagacion_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_indagacion_terminacion_archivo_del_expediente = true
         elsif complaint.etapa_id = 3
           complaint.ordinario_apertura_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_apertura_terminacion_archivo_del_expediente = true
         elsif complaint.etapa_id = 4
           complaint.ordinario_prorroga_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_prorroga_terminacion_archivo_del_expediente = true
         elsif complaint.etapa_id = 5
           complaint.ordinario_cierre_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_cierre_terminacion_archivo_del_expediente = true
         elsif complaint.etapa_id = 7
           complaint.ordinario_descargos_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_descargos_terminacion_archivo_del_expediente = true
         elsif complaint.etapa_id = 8
           complaint.ordinario_alegatos_terminacion_fecha_actuacion = oo.cell(line,'X')
           complaint.ordinario_alegatos_terminacion_archivo_del_expediente = true
         end 
         
         end
         
        
         
         complaint.save!
         
         ##Migracion de abogados asignados
         
         if abogados[oo.cell(line,'AK')]
           users_complaint = UsersComplaint.new
           users_complaint.user_id = abogados[oo.cell(line,'AK')]
           users_complaint.complaint_id = complaint.id
           users_complaint.save
         else
           complaint.nombre_de_abogado_asignado = oo.cell(line,'AK')
           complaint.save
         end
         
       rescue
         
         puts "Error en fila #{rows}"
         
       end
         
    end
    puts "/////////////////////////////"
    puts "/////////////////////////////"
  end
  
  
  def create_users
    
    puts "Entro en create users"
    
    if Rails.env.development?
      urldef = "http://localhost:3000"+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
    else  
      urldef = GobVar.meta("domain")+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
      #urldef = "http://boyaca.ozonegroup.co"+"/"+self.avatar.url.gsub(/\?([0-9]*)/, '')
    end
    
    roles = Role.all
    roles_hash = Hash.new
    for role in roles
      roles_hash["#{role.name}"] = role.id
    end
   
    oo =  Roo::Excel.new(urldef) 
  #  oo.default_sheet = oo.sheets[1]
    rows = 1
    cont =1
    2.upto(oo.last_row) do |line|
      
      #begin
        @user = User.new
        
        @user.first_name = oo.cell(line,'B')
        @user.email = oo.cell(line,'E')
        @user.role_id = roles_hash[oo.cell(line,'D')]
        pass = "goboyaca#{cont}"
        @user.first_password = pass
        @user.password = pass
        @user.password_confirmation = pass
        @user.save
        
        puts "A: #{oo.cell(line,'A')}"
        puts "B: #{oo.cell(line,'B')}"
        puts "C: #{oo.cell(line,'C')}"
        puts "D: #{oo.cell(line,'D')}"
        puts "E: #{oo.cell(line,'E')}"
      #rescue
       # puts "error creando"
      #end
      cont = cont + 1
    end
  end
  
  
end
