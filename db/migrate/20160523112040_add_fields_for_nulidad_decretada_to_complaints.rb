class AddFieldsForNulidadDecretadaToComplaints < ActiveRecord::Migration
  def change
    
    add_column :complaints, :ordinario_nulidad_accion, :string
    add_column :complaints, :verbal_nulidad_accion, :string

  end
end
