class AddConsecutivoToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :consecutivo, :integer
  end
end
