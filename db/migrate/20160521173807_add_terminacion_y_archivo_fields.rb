class AddTerminacionYArchivoFields < ActiveRecord::Migration
 def change
   
   add_column :complaints, :ordinario_indagacion_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_indagacion_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_indagacion_terminacion_archivo_confirma_notificacion, :datetime
   
   add_column :complaints, :ordinario_apertura_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_apertura_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_apertura_terminacion_archivo_confirma_notificacion, :datetime
   
   add_column :complaints, :ordinario_prorroga_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_prorroga_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_prorroga_terminacion_archivo_confirma_notificacion, :datetime
   
   add_column :complaints, :ordinario_cierre_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_cierre_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_cierre_terminacion_archivo_confirma_notificacion, :datetime
   
   add_column :complaints, :ordinario_descargos_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_descargos_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_descargos_terminacion_archivo_confirma_notificacion, :datetime
   
   add_column :complaints, :ordinario_alegatos_terminacion_archivo_confirma_actuacion, :datetime
   add_column :complaints, :ordinario_alegatos_terminacion_archivo_confirma_comunicacion, :datetime
   add_column :complaints, :ordinario_alegatos_terminacion_archivo_confirma_notificacion, :datetime    
   
 end
end
