class AddFieldNulidadEnToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :nulidad_en, :string
    add_column :complaints, :parent_id, :integer
    add_column :complaints, :anulada, :boolean 
  end
end
