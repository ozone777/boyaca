class AddFieldsv3ToComplaints < ActiveRecord::Migration
  def change
    
    #Evaluacion noticia disciplinaria
    add_column :complaints, :ordinario_evaluacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_evaluacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_evaluacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_evaluacion_autoinhibitorio, :boolean
    add_column :complaints, :ordinario_evaluacion_remision_por_competencia, :boolean
    add_column :complaints, :ordinario_evaluacion_citacion_audiencia, :boolean
    add_column :complaints, :ordinario_evaluacion_indagacion_preliminar, :boolean
    add_column :complaints, :ordinario_evaluacion_apertura_investigacion, :boolean
    
    add_column :complaints, :ordinario_evaluacion_nulidad, :boolean
    add_column :complaints, :ordinario_evaluacion_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_evaluacion_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_evaluacion_nulidad_fecha_notificacion, :datetime
    
    #Indagacion preliminar
    add_column :complaints, :ordinario_indagacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_indagacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_indagacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_indagacion_citacion_audiencia, :boolean
    add_column :complaints, :ordinario_indagacion_apertura_investigacion, :boolean
    
    add_column :complaints, :ordinario_indagacion_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_indagacion_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_indagacion_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_indagacion_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_indagacion_nulidad, :boolean
    add_column :complaints, :ordinario_indagacion_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_indagacion_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_indagacion_nulidad_fecha_notificacion, :datetime
    
    #Apertura investigacion disciplinaria
    add_column :complaints, :ordinario_apertura_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_apertura_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_apertura_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_apertura_prorroga_de_la_ivestigacion, :boolean
    add_column :complaints, :ordinario_apertura_auto_cierre_de_investigacion, :boolean
    add_column :complaints, :ordinario_apertura_prorroga, :boolean
    
    add_column :complaints, :ordinario_apertura_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_apertura_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_apertura_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_apertura_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_apertura_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_apertura_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_apertura_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_apertura_nulidad, :boolean
    add_column :complaints, :ordinario_apertura_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_apertura_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_apertura_nulidad_fecha_notificacion, :datetime
    
    #Prorroga de la investigacion disciplinaria
    add_column :complaints, :ordinario_prorroga_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_prorroga_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_prorroga_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_prorroga_auto_cierre_de_la_investigacion, :boolean
    
    add_column :complaints, :ordinario_prorroga_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_prorroga_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_prorroga_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_prorroga_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_prorroga_nulidad, :boolean
    add_column :complaints, :ordinario_prorroga_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_prorroga_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_prorroga_nulidad_fecha_notificacion, :datetime
    
    #Cierre de la investigacion
    add_column :complaints, :ordinario_cierre_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_cierre_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_cierre_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_cierre_pliego_de_cargos, :boolean
    
    add_column :complaints, :ordinario_cierre_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_cierre_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_cierre_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_cierre_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_cierre_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_cierre_terminacion_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_cierre_terminacion_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_cierre_nulidad, :boolean
    add_column :complaints, :ordinario_cierre_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_cierre_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_cierre_nulidad_fecha_notificacion, :datetime
    
    #Pliego de cargos
    add_column :complaints, :ordinario_pliego_de_cargos_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_segunda_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_segunda_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_segunda_fecha_notificacion, :datetime
    
    add_column :complaints, :ordinario_pliego_de_cargos_nulidad, :boolean
    add_column :complaints, :ordinario_pliego_de_cargos_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_pliego_de_cargos_nulidad_fecha_notificacion, :datetime
    
    #Descargos
    add_column :complaints, :ordinario_descargos_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_descargos_variacion_pliego_de_cargos, :boolean
    add_column :complaints, :ordinario_descargos_alegatos_de_conclusion, :boolean
    
    add_column :complaints, :ordinario_descargos_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_descargos_recurso_de_apelacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_recurso_de_apelacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_recurso_de_apelacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_descargos_confirma, :boolean
    add_column :complaints, :ordinario_descargos_revoca, :boolean
    
    add_column :complaints, :ordinario_descargos_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_descargos_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_descargos_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_descargos_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_descargos_terminacion_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_descargos_nulidad, :boolean
    add_column :complaints, :ordinario_descargos_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_nulidad_fecha_notificacion, :datetime
    
    #Alegatos de conclusion
    add_column :complaints, :ordinario_alegatos_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_alegatos_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_alegatos_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_alegatos_fallo_en_primera_instancia, :boolean
    
    add_column :complaints, :ordinario_alegatos_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :ordinario_alegatos_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_alegatos_terminacion_terminacion_confirma, :boolean
    add_column :complaints, :ordinario_alegatos_terminacion_terminacion_revoca, :boolean
    
    add_column :complaints, :ordinario_alegatos_nulidad, :boolean
    add_column :complaints, :ordinario_alegatos_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_alegatos_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_alegatos_nulidad_fecha_notificacion, :datetime
    
    #Fallo en primera instancia
    add_column :complaints, :ordinario_fallo_en_primera_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_fecha_notificacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_fallo_sancionatorio, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_fallo_absolutorio, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_apelar, :boolean
    
    add_column :complaints, :ordinario_fallo_en_primera_nulidad, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_fecha_notificacion, :datetime
    
    #Fallo segunda instancia
    add_column :complaints, :ordinario_fallo_en_segunda_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_fecha_notificacion, :datetime
    
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad, :boolean
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_fecha_notificacion, :datetime
    
  end
end
