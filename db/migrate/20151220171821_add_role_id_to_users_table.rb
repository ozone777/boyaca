class AddRoleIdToUsersTable < ActiveRecord::Migration
  def change
  	rename_column :users, :privilege_id, :role_id
  end
end
