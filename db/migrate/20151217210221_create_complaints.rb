class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.datetime :fecha_hechos
      t.integer :estado_proceso
      t.datetime :fecha_vencimiento_terminos
      t.text :comentarios_jefe_oficina
      t.text :texto_indagacion_preliminar
      t.integer :comaplaint_type_id
      t.integer :auto_acomulatorio_id
      t.text :comentario_auto_inhibitorio

      t.timestamps
    end
  end
end
