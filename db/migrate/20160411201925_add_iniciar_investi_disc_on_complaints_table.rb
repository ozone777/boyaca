class AddIniciarInvestiDiscOnComplaintsTable < ActiveRecord::Migration
  def change
    add_column :complaints, :iniciar_investigacion_disciplinaria, :boolean, :after => :comentarios_indagacion_preliminar
    add_column :complaints, :cierre_investigacion_disciplinaria, :boolean, :after => :iniciar_investigacion_disciplinaria
  end
end
