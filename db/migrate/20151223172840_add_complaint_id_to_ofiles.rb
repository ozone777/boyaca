class AddComplaintIdToOfiles < ActiveRecord::Migration
  def change
    add_column :ofiles, :complaint_id, :integer
  end
end
