class AddAndRemoveFieldsInComplaintsTable < ActiveRecord::Migration
  def up
  	rename_column :complaints, :comentario_auto_inhibitorio, :argumentos_auto_inhibitorio
  	add_column :complaints, :argumentos_revision_competencia, :text, :null => true, :after => :argumentos_auto_inhibitorio
  	add_column :complaints, :comentarios_indagacion_preliminar, :text, :null => true, :after => :argumentos_revision_competencia
  	add_column :complaints, :viable, :boolean, :null => true, :after => :comentarios_indagacion_preliminar
  	add_column :complaints, :comentarios_jefe_oficina_investigacion_disciplinaria, :text, :null => true, :after => :viable
  	add_column :complaints, :es_culpable, :boolean, :null => true, :after => :comentarios_jefe_oficina_investigacion_disciplinaria
  	add_column :complaints, :nombre_implicado, :string, :null => true, :after => :es_culpable
  	add_column :complaints, :identificacion_implicado, :string, :null => true, :after => :nombre_implicado
  	add_column :complaints, :cargo_implicado, :string, :null => true, :after => :identificacion_implicado
  	add_column :complaints, :fecha_decision_tomada, :datetime, :null => true, :after => :cargo_implicado
  	add_column :complaints, :se_presento_a_notificacion_personal, :boolean, :null => true, :after => :fecha_decision_tomada
  	add_column :complaints, :nombre_abogado_asignado, :string, :null => true, :after => :se_presento_a_notificacion_personal
  	add_column :complaints, :identificacion_abogado_asignado, :string, :null => true, :after => :nombre_abogado_asignado
  	add_column :complaints, :n_tarjeta_profesional_abogado_asignado, :string, :null => true, :after => :identificacion_abogado_asignado
  	add_column :complaints, :solicitud_nulidad, :boolean, :null => true, :after => :n_tarjeta_profesional_abogado_asignado
  	add_column :complaints, :fecha_dictado_sentencia, :datetime, :null => true, :after => :solicitud_nulidad
  	add_column :complaints, :fecha_ejecucion, :datetime, :null => true, :after => :fecha_dictado_sentencia
  	add_column :complaints, :fecha_limite_ejecucion, :datetime, :null => true, :after => :fecha_ejecucion
  	add_column :complaints, :apelacion, :boolean, :null => true, :after => :fecha_limite_ejecucion
  	add_column :complaints, :resultado_apelacion, :text, :null => true, :after => :apelacion
  end

  def down
  end
end
