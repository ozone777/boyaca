class AddNombreDeAbogadoAsignadoToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :nombre_de_abogado_asignado, :string
  end
end
