class AddFirstPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_password, :string
  end
end
