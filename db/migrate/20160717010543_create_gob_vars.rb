class CreateGobVars < ActiveRecord::Migration
  def change
    create_table :gob_vars do |t|
      t.string :meta_key
      t.text :meta_value

      t.timestamps
    end
  end
end
