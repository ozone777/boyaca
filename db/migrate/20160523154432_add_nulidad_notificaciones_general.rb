class AddNulidadNotificacionesGeneral < ActiveRecord::Migration
  
  def change
  
    add_column :complaints, :ordinario_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_nulidad_fecha_notificacion, :datetime
    
  end
  
end
