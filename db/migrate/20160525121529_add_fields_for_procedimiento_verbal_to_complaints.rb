class AddFieldsForProcedimientoVerbalToComplaints < ActiveRecord::Migration
  def change
    
    #etapa 1 citacion
    add_column :complaints, :verbal_citacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_citacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_citacion_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_citacion_nulidad, :boolean
    add_column :complaints, :verbal_citacion_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_citacion_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_citacion_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_citacion_nulidad_recurso, :boolean
    add_column :complaints, :verbal_citacion_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_citacion_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_citacion_nulidad_recurso_notificacion, :datetime
    add_column :complaints, :verbal_citacion_nulidad_decretada, :string
    
    #Etapa 2 primera audiencia
    add_column :complaints, :verbal_primera_fecha_actuacion, :datetime
    add_column :complaints, :verbal_primera_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_primera_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_primera_nulidad, :boolean
    add_column :complaints, :verbal_primera_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_primera_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_primera_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_primera_nulidad_recurso, :boolean
    add_column :complaints, :verbal_primera_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_primera_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_primera_nulidad_recurso_notificacion, :datetime
    add_column :complaints, :verbal_primera_nulidad_decretada, :string
    
    add_column :complaints, :verbal_primera_alegatos, :boolean
    add_column :complaints, :verbal_primera_alegatos_fecha_actuacion, :datetime
    add_column :complaints, :verbal_primera_alegatos_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_primera_alegatos_fecha_notificacion, :datetime
    
    #Etapa 3 Segunda audiencia
    add_column :complaints, :verbal_segunda_fecha_actuacion, :datetime
    add_column :complaints, :verbal_segunda_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_segunda_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_segunda_apelacion, :boolean
    add_column :complaints, :verbal_segunda_apelacion_actuacion, :datetime
    add_column :complaints, :verbal_segunda_apelacion_comunicacion, :datetime
    add_column :complaints, :verbal_segunda_apelacion_notificacion, :datetime
    add_column :complaints, :verbal_segunda_apelacion_decretada, :string
    
    add_column :complaints, :verbal_segunda_nulidad, :boolean
    add_column :complaints, :verbal_segunda_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_segunda_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_segunda_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_segunda_nulidad_recurso, :boolean
    add_column :complaints, :verbal_segunda_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_segunda_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_segunda_nulidad_recurso_notificacion, :datetime
    add_column :complaints, :verbal_segunda_nulidad_decretada, :string
 

  end
end
