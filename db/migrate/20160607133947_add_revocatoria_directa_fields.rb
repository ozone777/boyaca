class AddRevocatoriaDirectaFields < ActiveRecord::Migration
  
  def change
    
    add_column :complaints, :ordinario_indagacion_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_indagacion_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_indagacion_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_indagacion_revocatoria_directa_fecha_notificacion, :datetime
    
    
    add_column :complaints, :ordinario_apertura_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_apertura_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_apertura_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_apertura_revocatoria_directa_fecha_notificacion, :datetime
    
    
    add_column :complaints, :ordinario_prorroga_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_prorroga_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_prorroga_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_prorroga_revocatoria_directa_fecha_notificacion, :datetime
    
    
    add_column :complaints, :ordinario_cierre_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_cierre_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_cierre_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_cierre_revocatoria_directa_fecha_notificacion, :datetime
    
    add_column :complaints, :ordinario_descargos_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_descargos_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_revocatoria_directa_fecha_notificacion, :datetime
    
    
    add_column :complaints, :ordinario_alegatos_revocatoria_directa, :boolean
    add_column :complaints, :ordinario_alegatos_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_alegatos_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_alegatos_revocatoria_directa_fecha_notificacion, :datetime
    
  end
  
end
