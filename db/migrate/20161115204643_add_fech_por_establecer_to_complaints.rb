class AddFechPorEstablecerToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :fecha_de_los_hechos_por_establecer, :boolean
  end
end
