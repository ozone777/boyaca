class AddFieldsToComplaintsCloneEtapas < ActiveRecord::Migration
  def change
    
    add_column :complaints, :verbal_evaluacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_evaluacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_evaluacion_fecha_notificacion, :datetime
    add_column :complaints, :verbal_evaluacion_autoinhibitorio, :boolean
    add_column :complaints, :verbal_evaluacion_remision_por_competencia, :boolean
    add_column :complaints, :verbal_evaluacion_citacion_audiencia, :boolean
    add_column :complaints, :verbal_evaluacion_indagacion_preliminar, :boolean
    add_column :complaints, :verbal_evaluacion_apertura_investigacion, :boolean
    
    add_column :complaints, :verbal_evaluacion_nulidad, :boolean
    add_column :complaints, :verbal_evaluacion_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_evaluacion_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_evaluacion_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_evaluacion_nulidad_recurso, :boolean
    add_column :complaints, :verbal_evaluacion_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_evaluacion_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_evaluacion_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :verbal_evaluacion_nulidad_decretada, :string
    
    
    #Indagacion preliminar
    add_column :complaints, :verbal_indagacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_fecha_notificacion, :datetime
    add_column :complaints, :verbal_indagacion_citacion_audiencia, :boolean
    add_column :complaints, :verbal_indagacion_apertura_investigacion, :boolean
    
    add_column :complaints, :verbal_indagacion_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :verbal_indagacion_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :verbal_indagacion_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :verbal_indagacion_terminacion_confirma, :boolean
    add_column :complaints, :verbal_indagacion_terminacion_revoca, :boolean
    
    add_column :complaints, :verbal_indagacion_nulidad, :boolean
    add_column :complaints, :verbal_indagacion_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_indagacion_terminacion_archivo_confirma_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_terminacion_archivo_confirma_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_terminacion_archivo_confirma_notificacion, :datetime
    
    add_column :complaints, :verbal_indagacion_nulidad_recurso, :boolean
    add_column :complaints, :verbal_indagacion_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :verbal_indagacion_nulidad_decretada, :string
    add_column :complaints, :verbal_indagacion_revocatoria_directa, :boolean
    add_column :complaints, :verbal_indagacion_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :verbal_indagacion_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_indagacion_revocatoria_directa_fecha_notificacion, :datetime
    
    #Apertura investigacion disciplinaria
    add_column :complaints, :verbal_apertura_fecha_actuacion, :datetime
    add_column :complaints, :verbal_apertura_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_fecha_notificacion, :datetime
    add_column :complaints, :verbal_apertura_prorroga_de_la_ivestigacion, :boolean
    add_column :complaints, :verbal_apertura_auto_cierre_de_investigacion, :boolean
    add_column :complaints, :verbal_apertura_prorroga, :boolean
    
    add_column :complaints, :verbal_apertura_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :verbal_apertura_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :verbal_apertura_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_apertura_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :verbal_apertura_terminacion_confirma, :boolean
    add_column :complaints, :verbal_apertura_terminacion_revoca, :boolean
    
    add_column :complaints, :verbal_apertura_nulidad, :boolean
    add_column :complaints, :verbal_apertura_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_apertura_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_apertura_terminacion_archivo_confirma_actuacion, :datetime
    add_column :complaints, :verbal_apertura_terminacion_archivo_confirma_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_terminacion_archivo_confirma_notificacion, :datetime
    
    add_column :complaints, :verbal_apertura_nulidad_recurso, :boolean
    add_column :complaints, :verbal_apertura_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_apertura_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :verbal_apertura_nulidad_decretada, :string
    
    add_column :complaints, :verbal_alegatos_revocatoria_directa, :boolean
    add_column :complaints, :verbal_alegatos_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :verbal_alegatos_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_alegatos_revocatoria_directa_fecha_notificacion, :datetime
    

    #Prorroga de la investigacion disciplinaria
    add_column :complaints, :verbal_prorroga_fecha_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_fecha_notificacion, :datetime
    add_column :complaints, :verbal_prorroga_auto_cierre_de_la_investigacion, :boolean
    
    add_column :complaints, :verbal_prorroga_terminacion_archivo_del_expediente, :boolean
    add_column :complaints, :verbal_prorroga_terminacion_recurso_de_apelacion, :boolean
    add_column :complaints, :verbal_prorroga_terminacion_fecha_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_terminacion_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_terminacion_fecha_notificacion, :datetime
    add_column :complaints, :verbal_prorroga_terminacion_confirma, :boolean
    add_column :complaints, :verbal_prorroga_terminacion_revoca, :boolean
    
    add_column :complaints, :verbal_prorroga_nulidad, :boolean
    add_column :complaints, :verbal_prorroga_nulidad_fecha_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_nulidad_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_nulidad_fecha_notificacion, :datetime
    
    add_column :complaints, :verbal_prorroga_terminacion_archivo_confirma_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_terminacion_archivo_confirma_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_terminacion_archivo_confirma_notificacion, :datetime
    
    add_column :complaints, :verbal_prorroga_nulidad_recurso, :boolean
    add_column :complaints, :verbal_prorroga_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :verbal_prorroga_nulidad_decretada, :string
    
    add_column :complaints, :verbal_prorroga_revocatoria_directa, :boolean
    add_column :complaints, :verbal_prorroga_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :verbal_prorroga_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_prorroga_revocatoria_directa_fecha_notificacion, :datetime
    
 
  end
end
