class AddTramitadaToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :estado_de_tramite, :string
  end
end
