class AddDecretoDePruebasFieldsToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :verbal_decreto_de_pruebas, :boolean
    
    add_column :complaints, :verbal_decreto_de_pruebas_fecha_actuacion, :datetime
    add_column :complaints, :verbal_decreto_de_pruebas_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_decreto_de_pruebas_fecha_notificacion, :datetime
    
  end
end
