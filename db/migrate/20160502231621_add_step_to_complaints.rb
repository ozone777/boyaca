class AddStepToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :step, :string
  end
end
