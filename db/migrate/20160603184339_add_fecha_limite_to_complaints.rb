class AddFechaLimiteToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :fecha_limite, :datetime
  end
end
