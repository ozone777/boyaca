class AddLastGeneralFieldsToComplaints < ActiveRecord::Migration
  def change
    
    add_column :complaints, :fecha_queja, :date
    add_column :complaints, :fecha_recibido_en_esta_dependencia, :date
    add_column :complaints, :cedula_disciplinado, :string
    add_column :complaints, :entidad, :string
    add_column :complaints, :municipio, :string
    add_column :complaints, :disciplinado, :text
    
  end
end
