class AddDecretadaNulidadFieldsToComplaints < ActiveRecord::Migration
  
  def change
    
    add_column :complaints, :ordinario_evaluacion_nulidad_decretada, :string
    
    
    add_column :complaints, :ordinario_indagacion_nulidad_decretada, :string
    
    
    add_column :complaints, :ordinario_apertura_nulidad_decretada, :string
    
    
    add_column :complaints, :ordinario_prorroga_nulidad_decretada, :string
   
    
    add_column :complaints, :ordinario_cierre_nulidad_decretada, :string
    
    
    add_column :complaints, :ordinario_pliego_nulidad_decretada, :string
   
    
    add_column :complaints, :ordinario_descargos_nulidad_decretada, :string
   
   
    add_column :complaints, :ordinario_alegatos_nulidad_decretada, :string
    
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_decretada, :string
    
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_decretada, :string
    
  end
  
end
