class CreateUsersComplaints < ActiveRecord::Migration
  def change
    create_table :users_complaints do |t|
      t.integer :user_id
      t.integer :complaint_id

      t.timestamps
    end
  end
end
