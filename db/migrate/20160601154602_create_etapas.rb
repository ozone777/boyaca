class CreateEtapas < ActiveRecord::Migration
  def change
    create_table :etapas do |t|
      t.string :name
      t.string :tipo

      t.timestamps
    end
  end
end
