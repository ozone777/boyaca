class AddComplaintsFechasString < ActiveRecord::Migration
 
  def change
    add_column :complaints, :fecha_queja_string, :text
    add_column :complaints, :fecha_recibido_en_esta_dependencia_string, :text
    add_column :complaints, :fecha_hechos_string, :text
  end
end
