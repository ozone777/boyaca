class CreateOfiles < ActiveRecord::Migration
  def change
    create_table :ofiles do |t|
      t.string :name

      t.timestamps
    end
  end
end
