class CreateGobDocs < ActiveRecord::Migration
  def change
    create_table :gob_docs do |t|
      t.string :name
      t.string :dpto

      t.timestamps
    end
  end
end
