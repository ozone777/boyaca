class AddTerminacionYArchivoApDatesToComplaints < ActiveRecord::Migration
  def change
    
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_indagacion_terminacion_fecha_notificacion_ap, :datetime
    
    add_column :complaints, :ordinario_apertura_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_apertura_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_apertura_terminacion_fecha_notificacion_ap, :datetime
    
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_prorroga_terminacion_fecha_notificacion_ap, :datetime
    
    add_column :complaints, :ordinario_cierre_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_cierre_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_cierre_terminacion_fecha_notificacion_ap, :datetime
    
    add_column :complaints, :ordinario_descargos_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_descargos_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_descargos_terminacion_fecha_notificacion_ap, :datetime
    
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_actuacion_ap, :datetime
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_comunicacion_ap, :datetime
    add_column :complaints, :ordinario_alegatos_terminacion_fecha_notificacion_ap, :datetime
    
    
    

    
  end
end
