class AddMoreFieldsToClone < ActiveRecord::Migration
  def change
    
    add_column :complaints, :verbal_apertura_revocatoria_directa, :boolean
    add_column :complaints, :verbal_apertura_revocatoria_directa_fecha_actuacion, :datetime
    add_column :complaints, :verbal_apertura_revocatoria_directa_fecha_comunicacion, :datetime
    add_column :complaints, :verbal_apertura_revocatoria_directa_fecha_notificacion, :datetime
    
  end
end
