class AddTokenFromUser < ActiveRecord::Migration
  def change
  	add_column :users, :token, :string, :null =>  true, :after => :email
  end
end
