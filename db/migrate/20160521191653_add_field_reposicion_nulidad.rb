class AddFieldReposicionNulidad < ActiveRecord::Migration
 
  def change
    
    add_column :complaints, :ordinario_evaluacion_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_evaluacion_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_evaluacion_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_evaluacion_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_indagacion_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_indagacion_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_indagacion_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_indagacion_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_apertura_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_apertura_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_apertura_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_apertura_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_prorroga_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_prorroga_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_prorroga_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_prorroga_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_cierre_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_cierre_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_cierre_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_cierre_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_pliego_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_pliego_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_pliego_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_pliego_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_descargos_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_descargos_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_descargos_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_descargos_nulidad_recurso_notificacion, :datetime
   
    add_column :complaints, :ordinario_alegatos_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_alegatos_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_alegatos_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_alegatos_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_nulidad_recurso_notificacion, :datetime
    
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_recurso, :boolean
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_recurso_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_recurso_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_segunda_nulidad_recurso_notificacion, :datetime

    
  end
  
end
