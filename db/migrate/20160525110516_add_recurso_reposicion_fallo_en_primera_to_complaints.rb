class AddRecursoReposicionFalloEnPrimeraToComplaints < ActiveRecord::Migration
  def change
    
    add_column :complaints, :ordinario_fallo_en_primera_recurso, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_recurso_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_recurso_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_recurso_fecha_notificacion, :datetime
    
    add_column :complaints, :ordinario_fallo_en_primera_revocatoria, :boolean
    add_column :complaints, :ordinario_fallo_en_primera_revocatoria_fecha_actuacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_revocatoria_fecha_comunicacion, :datetime
    add_column :complaints, :ordinario_fallo_en_primera_revocatoria_fecha_notificacion, :datetime
    
  end
end
