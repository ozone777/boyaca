class AddArchivadoToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :archivado, :boolean
  end
end
