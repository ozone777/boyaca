class AddAvatarToGobDocs < ActiveRecord::Migration
  def up
     add_attachment :gob_docs, :avatar
   end

   def down
     remove_attachment :gob_docs, :avatar
   end
end
