class AddFieldsV2ToComplaints < ActiveRecord::Migration
  def change
    
    #Tipos de proceso
    add_column :complaints, :tipo_de_proceso, :string
    
    add_column :complaints, :nombre_del_implicado, :string
    add_column :complaints, :cargo_del_implicado, :string
    add_column :complaints, :lugar_de_los_hechos, :string
    
    add_column :complaints, :origen_de_queja, :text
    
    add_column :complaints, :conducta_a_investigar, :text
    
  end
end
