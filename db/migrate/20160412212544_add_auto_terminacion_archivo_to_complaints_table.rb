class AddAutoTerminacionArchivoToComplaintsTable < ActiveRecord::Migration
  def change
    add_column :complaints, :auto_terminacion_archivo, :boolean, :after => :resultado_apelacion
  end
end
