class AddFieldsTyperVerbalToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :fecha_cita_audiencia, :datetime, :after => :auto_acomulatorio_id
    add_column :complaints, :apuntes_audiencia, :text, :after => :fecha_cita_audiencia
    add_column :complaints, :audiencia_prorrogada, :boolean, :after => :apuntes_audiencia
    add_column :complaints, :comentarios_decision_culpabilidad, :text, :after => :es_culpable
    add_column :complaints, :decision_final_ejecutoriada, :boolean, :after => :comentarios_decision_culpabilidad
    add_column :complaints, :se_interponen_recursos, :boolean, :after => :decision_final_ejecutoriada
    add_column :complaints, :comentarios_interponen_recursos, :text, :after => :se_interponen_recursos
    add_column :complaints, :remitir_procedimiento_ordinario, :boolean, :after => :comentarios_interponen_recursos
  end
end
