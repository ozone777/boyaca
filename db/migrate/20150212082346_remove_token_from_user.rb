class RemoveTokenFromUser < ActiveRecord::Migration
  def up
  	remove_column :users, :token
  end

  def down
  end
end
