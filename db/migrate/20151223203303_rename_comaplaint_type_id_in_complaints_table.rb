class RenameComaplaintTypeIdInComplaintsTable < ActiveRecord::Migration
  def change
  	rename_column :complaints, :comaplaint_type_id, :complaint_type_id
  end
end
