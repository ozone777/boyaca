class AddPaperfieldsToOfile < ActiveRecord::Migration
  def up
      add_attachment :ofiles, :avatar
    end

    def down
      remove_attachment :ofiles, :avatar
    end
end
